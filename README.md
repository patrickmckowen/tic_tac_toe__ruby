


Directory Structure:

 |---main.rb
 |
 |---tests
 |     |--main.rb
 |
 |---ttt
       |--game
       |    |--game.rb
       |
       |--board
       |    |--board.rb
       |
       |--player
            |--player.rb
            |
            |--human
            |    |--human.rb
            |
            |--robot
                 |--sequential
                 |     |--sequential.rb
                 |  
                 |--random
                 |     |--random.rb
                 |
                 |--unbeatable
                       |--unbeatable.rb
        