# --------------------------------------------------------------------------- #
                           # IMPORTS #

require 'pg'
require 'bcrypt'
load "./local_env.rb" if File.exists?("./local_env.rb")

# --------------------------------------------------------------------------- #
                    # DATABASE CONFIGURATION #

db_params = {
	host: ENV['host'],
	port: ENV['port'],
	dbname: ENV['dbname'],
	user: ENV['user'],
	password: ENV['password']
}

# ------------------------------------------------------------------------- #
                      # Instantiate Connections #
#
$db = PG::Connection.new(db_params)

# ------------------------------------------------------------------------- #
                      # Create table Query #
                      # (Only needed once) #
                      
def create_table()
	$db.exec("CREATE TABLE tic_tac_toe(
		ID 	 SERIAL    PRIMARY KEY,
		player1_name           TEXT,
		player2_names          TEXT,
		winner        	       TEXT,
		datetime	           TEXT
		);")
end

# --------------------------------------------------------------------------- #
                         # INSERT Game #
#
def insert_game(p1_name, p2_name, winner)
        $db.exec("INSERT INTO tic_tac_toe (player1_name, player2_names, winner, datetime) 
        VALUES ('#{p1_name}', '#{p2_name}', '#{winner}', '#{Time.new.inspect}')")
end

# --------------------------------------------------------------------------- #
					# GENERATE HTML TABLE #
					
def generate_table()
	response_obj = $db.exec("SELECT * FROM tic_tac_toe")
	html = ''
	html << "<table sytle=\"border: 2px solid black;\">
	<tr>
	    <td>ID</td>
	    <td>Player 1 Name</td>
	    <td>Player 2 Name</td>
	    <td>Winner</td>
	    <td>Date time</td>
	  </tr>"

    # GENERATE ROW
	response_obj.each do |row|
		html << "\t<tr>"
		row.each {|cell| html << "\t\t<td>#{cell[1]}</td>\n"}
		html << "\t</tr>"
	end
	# END TABLE
	html << "</table>"
	# RETURN FINAL STRING
	html
end

###############################################################################

