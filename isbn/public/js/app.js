<script>
document.getElementById("isbn_form").addEventListener("submit", function(event){
	event.preventDefault();
	isbn_form = document.getElementById("isbn_form")
	var formData = new FormData(isbn_form);
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
			document.getElementById("response_area").innerHTML = this.responseText;
    	}
	};
	xhttp.open("POST", "validate_isbn", true);
	xhttp.send(new FormData(isbn_form));
});
</script>
