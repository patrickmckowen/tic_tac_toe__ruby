require 'sinatra'
require 'csv'                    
require_relative 'functions.rb'
require_relative 's3.rb'        # S3 related functions
enable :sessions


######################################

# index
get '/' do
	erb :index
end

# AJAX FUNCTION
post '/validate_isbn' do
	write_csv('output.csv', params[:isbn])
	get_validation_message(params[:isbn])
# ------------------------------------------------------------- #
                  # SENDING FUNCTION #
	connect_to_s3()
# ------------------------------------------------------------- #

# ------------------------------------------------------------- #
                  # READING FUNCTION #
  read_from_s3()
# ------------------------------------------------------------- #
end

# 
get '/upload' do

  # Check if user uploaded a file AND the file has a name
  if params[:csv_file] && params[:csv_file][:filename]
  	
  	# VARIABLES: name of file, the file data itself, the dynamic upload url
    filename = params[:csv_file][:filename]
    file = params[:csv_file][:tempfile]
    path = "./public/uploads/#{filename}"  # UPLOADS DEST 

    # Write file to disk
    File.open(path, 'wb') do |f|
      f.write(file.read)
    end
    
    # Read from the uploaded file and write to the outputted file      
    process_csv_file(path, "./public/uploads/processed/#{filename}")
  end

  # ------------------------------------------------------------- #
                    # SENDING FUNCTION #
  connect_to_s3()
  # ------------------------------------------------------------- #

  # ------------------------------------------------------------- #
                  # READING FUNCTION #
  read_from_s3()
  # ------------------------------------------------------------- #

  redirect "/"
end

###################################################################

post '/update_table' do
	read_csv 'output.csv'
end

# File.delete(path) if File.exist?(path)
