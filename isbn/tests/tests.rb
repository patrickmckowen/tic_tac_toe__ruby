require "minitest/autorun"
require_relative "../functions.rb"

class Functions_test < Minitest::Test

# ---------------------------------------------------------------------------------------------- #
                                # Multiply Digits by index #

	def test_multiply_digits_1
		data = "1111111111111"
		goal = [1,3,1,3,1,3,1,3,1,3,1,3,1]
		assert_equal(goal, multiply_digits(data))
	end

	#
	def test_multiply_digits_2
		data = "9780470059029"
		goal = [9,21,8,0,4,21,0,0,5,27,0,6,9]
		assert_equal(goal, multiply_digits(data))
	end

	#
	def test_multiply_digits_3
		data = "978-0-13-149505-0"
		goal = [9, 21, 8, 0, 1, 9, 1, 12, 9, 15, 0, 15, 0]
		assert_equal(goal, multiply_digits(data))
	end

	#
	def test_multiply_digits_4
		data = "978 0 471 48648 0"
		goal = [9, 21, 8, 0, 4, 21, 1, 12, 8, 18, 4, 24, 0]
		assert_equal(goal, multiply_digits(data))
	end

# ---------------------------------------------------------------------------------------------- #
                                         # SUM #
	#
	def test_get_sum_13_1
		data = [1,3,1,3,1,3,1,3,1,3,1,3,1]
		goal = 24
		assert_equal(goal, get_sum_13(data))
	end

	#
	def test_get_sum_13_2
		data = [9,21,8,0,4,21,0,0,5,27,0,6,9]
		goal = 101
		assert_equal(goal, get_sum_13(data))
	end

# ---------------------------------------------------------------------------------------------- #
                                    # MULTIPLIES, SUMS #

	def test_multiply_and_add_digits_1
		data = "1111111111111"
		goal = 24
		assert_equal(goal, get_sum_13(multiply_digits(data)))
	end

	def test_multiply_and_add_digits_2
		data = "9780470059029"
		goal = 101
		assert_equal(goal, get_sum_13(multiply_digits(data)))
	end

	def test_multiply_and_add_digits_3
		data = "978-0-13-149505-0"
		goal = 100
		assert_equal(goal, get_sum_13(multiply_digits(data)))
	end

	def test_multiply_and_add_digits_4
		data = "978 0 471 48648 0"
		goal = 130
		assert_equal(goal, get_sum_13(multiply_digits(data)))
	end

# ---------------------------------------------------------------------------------------------- #
                                          # MODULUS #
	#
	def test_get_modulo_10__10
		data = 10
		goal = 0
		assert_equal(goal, get_modulo_10(data))
	end

	#
	def test_get_modulo_10__11
		data = 11
		goal = 1
		assert_equal(goal, get_modulo_10(data))
	end

# ---------------------------------------------------------------------------------------------- #

	                              # MULTIPLIES, SUMS, MODULO #

	# [1,3,1,3,1,3,1,3,1,3,1,3,1]
	# 25
	# 25 % 10 = 5
	def test_multiply_sum_modulo_1
		data = "1111111111111"
		goal = 4
		assert_equal(goal, get_modulo_10(get_sum_13(multiply_digits(data))))
	end

	# [9,21,8,0,4,21,0,0,5,27,0,6,9]
	# 110
	def test_multiply_sum_modulo_2
		data = "9780470059029" #
		goal = 1
		assert_equal(goal, get_modulo_10(get_sum_13(multiply_digits(data))))
	end

	# [9, 21, 8, 0, 1, 9, 1, 12, 9, 15, 0, 15, 0]
	# 100
	def test_multiply_sum_modulo_3
		data = "978-0-13-149505-0"
		goal = 0
		assert_equal(goal, get_modulo_10(get_sum_13(multiply_digits(data))))
	end

	# [9, 21, 8, 0, 4, 21, 1, 12, 8, 18, 4, 24, 0]
	# 130
	# 130 % 10 = 0
	def test_multiply_sum_modulo_4
		data = "978 0 471 48648 0"
		goal = 0
		assert_equal(goal, get_modulo_10(get_sum_13(multiply_digits(data))))
	end


# ---------------------------------------------------------------------------------------------- #

	                         # MULTIPLIES, SUMS, 10-MODULO

	def test_multiply_sum_10_minus_modulo
		data = "1111111111111"
		goal = 6
		assert_equal(goal, 10 - get_modulo_10(get_sum_13(multiply_digits(data))))
	end

# ---------------------------------------------------------------------------------------------- #

	                         # MULTIPLIES, SUMS, 10-MODULO % 10

	def test_multiply_sum_modulo_of_10_minus_modulo_1
		data = "1111111111111"
		goal = 6
		assert_equal(goal, get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(data)))))
	end

	# MULTIPLIES, SUMS, 10-MODULO % 10
	def test_multiply_sum_modulo_of_10_minus_modulo_2 #9780470059029
		data = "9780470059029"
		goal = 9
		assert_equal(goal, get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(data)))))
	end

	def test_multiply_sum_modulo_of_10_minus_modulo_3
		data = "978-0-13-149505-0"
		goal = 0
		assert_equal(goal, get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(data)))))
	end

	def test_multiply_sum_modulo_of_10_minus_modulo_4
		data = "978 0 471 48648 0"
		goal = 0
		assert_equal(goal, get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(data)))))
	end

	def test_multiply_sum_modulo_of_10_minus_modulo_5
		data = "978-1-56619-909-4"
		goal = 4
		assert_equal(goal, get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(data)))))
	end

	def test_multiply_sum_modulo_of_10_minus_modulo_6
		data = "978-3-16-148410-0"
		goal = 0
		assert_equal(goal, get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(data)))))
	end

	def test_multiply_sum_modulo_of_10_minus_modulo_7
		data = "978 0 471 48648 0"
		goal = 0
		assert_equal(goal, get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(data)))))
	end


# ---------------------------------------------------------------------------------------------- #
                                  # Calculating Check Digit #

	def test_calculated_check_digit_1
		data = "1111111111111"
		goal = 6
		assert_equal(goal, calculated_check_digit(data))
	end

	def test_calculated_check_digit_2
		data = "9780470059029"
		goal = 9
		assert_equal(goal, calculated_check_digit(data))
	end

	def test_calculated_check_digit_3
		data = "978-0-13-149505-0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end

	def test_calculated_check_digit_4
		data = "978 0 471 48648 0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end


	def test_calculated_check_digit_5
		data = "978-1-56619-909-4"
		goal = 4
		assert_equal(goal, calculated_check_digit(data))
	end


	def test_calculated_check_digit_6
		data = "978-3-16-148410-0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end

	
	def test_calculated_check_digit_7
		data = "978 0 471 48648 0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end

# ---------------------------------------------------------------------------------------------- #
                               # Get the calculated check digit #
	def test_calculated_check_digit_1
		data = "1111111111111"
		goal = 6
		assert_equal(goal, calculated_check_digit(data))
	end

	def test_calculated_check_digit_2
		data = "9780470059029"
		goal = 9
		assert_equal(goal, calculated_check_digit(data))
	end

	def test_calculated_check_digit_3
		data = "978-0-13-149505-0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end

	def test_calculated_check_digit_4
		data = "978 0 471 48648 0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end


	def test_calculated_check_digit_5
		data = "978-1-56619-909-4"
		goal = 4
		assert_equal(goal, calculated_check_digit(data))
	end


	def test_calculated_check_digit_6
		data = "978-3-16-148410-0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end

	
	def test_calculated_check_digit_7
		data = "978 0 471 48648 0"
		goal = 0
		assert_equal(goal, calculated_check_digit(data))
	end


# ---------------------------------------------------------------------------------------------- #
								# Retrieve the last check digit #
	def test_get_check_digit_1
		data = "978-1-56619-909-4"
		goal = 4
		assert_equal(goal, get_check_digit(data))
	end

# ---------------------------------------------------------------------------------------------- #
                                # Check digit Validation FUNCTIONS

	def test_check_digit_valid_1
		data = "978-1-56619-909-4"
		goal = true
		assert_equal(goal, check_digit_valid?(data))
	end

	def test_check_digit_valid_2
		data = "4780470059029"
		goal = false
		assert_equal(goal, check_digit_valid?(data))
	end


	def test_final_validate_13_1
		data = "978-1-56619-909-4"
		goal = true
		assert_equal(goal, final_validate_13(data))
	end

	def test_final_validate_13_2
		data = "4780470059029"
		goal = false
		assert_equal(goal, final_validate_13(data))
	end

	#
	def test_get_validation_message_1
		data = "978-1-56619-909-4"
		goal = "\"978-1-56619-909-4\" <br> <i>is</i> a valid isbn-13."
		assert_equal(goal, get_validation_message(data))
	end

	#
	def test_get_validation_message_2
		data = '4780470059029' 	
		goal = "\"4780470059029\" <br> <i>is not</i> a valid isbn-13."
		assert_equal(goal, get_validation_message(data))
	end

#
	def test_write_csv
		require 'csv'
		process_csv_file('example_csv.csv', 'test_csv.csv')

		expected_file_content =  " ISBN,Is Valid
9780470059029,valid
877195x869,invalid
978-0-13-149505-0,valid
0471958697,valid
"
		file_content = File.read('test_csv.csv')
		assert_equal(expected_file_content, file_content)
	end
end