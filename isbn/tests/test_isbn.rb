require "minitest/autorun"
require_relative "../isbn.rb"

class Functions_test < Minitest::Test

	# Test 1 - Remove spaces
	#    - These have no effect on anything. First to go.
	def test_without_spaces
		data = "0 321 14653 0"
		goal = "0321146530"
		assert_equal(goal, without_spaces(data))
	end

	# Test 2 - Same as with spaces
	def test_without_dashes
		data = "0-321-14653-0"
		goal = "0321146530"
		assert_equal(goal, without_dashes(data))
	end

	def test_is_i__true
		data = "0"
		goal = true
		assert_equal(goal, data.is_i?)
	end

	def test_is_i__true_3_digits
		data = "012"
		goal = true
		assert_equal(goal, data.is_i?)
	end

	def test_is_i__false
		data = "0j12"
		goal = false
		assert_equal(goal, data.is_i?)
	end

	def test_make_array_1
		data = "12345"
		goal = ['1','2','3','4','5']
		assert_equal(goal, make_array(data))
	end

	# Checking if my first funtion, that uses .scan, is the same as using .split
	def test_make_array_using_split
		data = "12345"
		goal = ['1','2','3','4','5']
		assert_equal(goal, make_array_using_split(data))
	end

	def test_integer_digit_count_1
		data = "0-321-14653-0"
		goal = 10
		assert_equal(goal, integer_digit_count(data))
	end

	def test_integer_digit_count_2
		data = "0-32321-1448653-0"
		goal = 14
		assert_equal(goal, integer_digit_count(data))
	end

	def test_multiply_by_index_1
		data = "12345"
		goal = [1,4,9,16,25]
		assert_equal(goal, multiply_by_index(data))
	end

	def test_multiply_by_index_2
		data = "54321"
		goal = [5,8,9,8,5]
		assert_equal(goal, multiply_by_index(data))
	end

	def test_multiply_by_index_arry
		data = ['5','4','3','2','1']
		goal = [5,8,9,8,5]
		assert_equal(goal, multiply_by_index(data))
	end

	def test_get_sum_15
		data = [1,2,3,4,5]
		goal = 15
		assert_equal(goal, get_sum(data))
	end

	def test_get_sum_6
		data = [1,2,3]
		goal = 6
		assert_equal(goal, get_sum(data))
	end

		def test_get_modulo_11__11
		data = 11
		goal = 0
		assert_equal(goal, get_modulo_11(data))
	end

	def test_get_modulo_11__12
		data = 12
		goal = 1
		assert_equal(goal, get_modulo_11(data))
	end

	def test_get_modulo_11__13
		data = 13
		goal = 2	
		assert_equal(goal, get_modulo_11(data))
	end

	def test_digit_is_x?
		data = "0-321-14653-2"
		goal = false	
		assert_equal(goal, last_digit_is_x?(data))
	end

	def test_digit_is_x?
		data = "0-321-14653-2x"
		goal = true	
		assert_equal(goal, last_digit_is_x?(data))
	end

	def test_digit_is_x?
		data = "0-321-14653-2X"
		goal = true	
		assert_equal(goal, last_digit_is_x?(data))
	end

	def test_validate_characters__valid
		data = "0471958697"
		goal = true	
		assert_equal(goal, validate_characters(data))
	end

	def test_validate_characters__valid_2
		data = "877195869x"
		goal = true	
		assert_equal(goal, validate_characters(data))
	end


	def test_final_validate__valid_1
		data = "0-321-14653-0"
		goal = true	
		assert_equal(goal, final_validate(data))
	end

	def test_final_validate__valid_2
		data = "877195869x"
		goal = true	
		assert_equal(goal, final_validate(data))
	end

	def test_final_validate__invalid_1
		data = "4780470059029"
		goal = false
		assert_equal(goal, final_validate(data))
	end

	def test_final_validate__invalid_2
		data = "0-321@14653-0"
		goal = false
		assert_equal(goal, final_validate(data))
	end

	def test_final_validate__invalid_3
		data = "877195x869"
		goal = false
		assert_equal(goal, final_validate(data))
	end

	# The length of the string, only counting valid digits
	def test_valid_length_10
		data = "877195869x"
		goal = 10
		assert_equal(goal, valid_length(data))
	end

	# Added an extra number to the previous tests
	def test_valid_length_11
		data = "1877195869x"
		goal = 11
		assert_equal(goal, valid_length(data))
	end

	# 
	def test_valid_length?
		data = "877195869x"
		goal = true
		assert_equal(goal, valid_length?(data))
	end

end
