

require 'rubygems'
require 'aws-sdk'
require 'csv'
#load "./local_env.rb"


def connect_to_s3()
	Aws::S3::Client.new(
		access_key_id: ENV['AWS_ACCESS_KEY_ID'],
    		secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    		region: ENV['AWS_REGION']
  	)
  	file = "output.csv"
  	bucket = 'mmtestbucket12345'
  	s3 = Aws::S3::Resource.new(region: ENV['AWS_REGION'])
  	obj = s3.bucket(bucket).object(file)

	# string data
	#obj.put(body: '"some code here to show something being added to the bucket."+ "\n"')

	# push entire file 
	File.open('output.csv', 'rb') do |file|
		obj.put(body: file)
	end
end

# -------------------------------------------------------------------------------- #


def read_from_s3()

	Aws::S3::Client.new(
		access_key_id: ENV['AWS_ACCESS_KEY_ID'],
    		secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    		region: ENV['AWS_REGION']
  	)

	# Construct a service client to make API calls
	s3 = Aws::S3::Client.new(region: ENV['AWS_REGION'])


	File.open('output.csv', 'wb') do |file|
		s3.get_object(bucket: 'mmtestbucket12345', key:'output.csv') do |chunk|
			file.write(chunk)
		end
	end

end
