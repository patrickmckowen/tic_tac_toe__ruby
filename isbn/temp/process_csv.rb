require 'csv'
require_relative 'functions.rb'
def process_csv_file(path, output_path)

	arr_of_arrs = CSV.read(path)

	# Add column to header
	arr_of_arrs[0] << "Is Valid"

	# for the remainder of the file, eval the second item and ad response
	arr_of_arrs[1..-1].each {|sub_arry| sub_arry << valid?(sub_arry[1]).to_s}

	# Write the data structure to the file
	CSV.open(output_path, "wb") {|csv| arr_of_arrs.each {|arry| csv << arry}}
end

# Test it!
#print process_csv_file('example_csv.csv', 'outputted_csv.csv')