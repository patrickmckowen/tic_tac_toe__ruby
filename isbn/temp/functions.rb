require 'csv'


# ----------------------------------------------------------------------------------------------- #
                               # Remove Pointless Characters #
#
def without_spaces(str)
	str.delete(' ')
end

#
def without_dashes(str)
	str.delete('-')
end

# ----------------------------------------------------------------------------------------------- #
                                  # Length Checking Functions #

# Gets length after pointless characters
def valid_length(isbn)
	without_dashes(without_spaces(isbn)).length
end 

# ISBN 10
def valid_length?(isbn)
	without_dashes(without_spaces(isbn)).length == 10
end

# ISBN 13
def valid_length_13?(isbn)
	without_dashes(without_spaces(isbn)).length == 13
end

# ----------------------------------------------------------------------------------------------- #
                                  # Character analysis #

# Check it string contains only numbers
class String
    def is_i?
    	# 3 equals means matching class too.
    	# \A matches start and end of string
    	# [-+] Either a - or a +
    	# \d means 1 digit (number)
    	# + Means one or more times
    	# \z matches end of string
    	# The thing has to be between //s
    	/\A[-+]?\d+\z/ === self
    end
end

# ISBN 10 has no illegal characters (Bad way)
def validate_characters_10(isbn)
	without_spaces = without_spaces(isbn)
	prepared_string = without_dashes(without_spaces)
	prepared_string = prepared_string.chop if last_digit_is_x?(prepared_string)
	prepared_string.is_i?
end

# Checks that there is no illegal characters (Good way) 13
def validate_characters(isbn)
	without_spaces(without_dashes(isbn)).is_i?
end

# ----------------------------------------------------------------------------------------------- #
                                  # Check digit calculation Functions #

#
def make_array(isbn)
	arry = isbn.to_s.scan /\w/  # \w is a special matching character that matches individual words
end

# ISBN 10
def multiply_by_index(isbn)
	temp_arry = []
	if isbn.class == String
		make_array(isbn).each_with_index {|digit, index| temp_arry << digit.to_i*(index + 1)}
	elsif isbn.class == Array
		isbn.each_with_index {|digit, index| temp_arry << digit.to_i*(index + 1)}
	end
	temp_arry
end

# ISBN 13
def multiply_digits(isbn)
	temp_arry = []
	if isbn.class == String
		make_array(isbn).each_with_index {|digit, index| (index + 1) % 2 == 0 ? (temp_arry << digit.to_i*3) : (temp_arry << digit.to_i)}
	elsif isbn.class == Array
		isbn.each_with_index {|digit, index| (index + 1) % 2 == 0 ? (temp_arry << digit.to_i*3) : (temp_arry << digit.to_i)}
	end
	temp_arry
end

# ISBN 10
def get_sum(arry)
	temp_int = 0
	arry.each {|i| temp_int += i.to_i}
	temp_int
end

# ISBN 13
def get_sum_13(arry)
	temp_int = 0
	arry[0..-2].each {|i| temp_int += i.to_i}
	temp_int
end

# ----------------------------------------------------------------------------------------------- #
                                  # Character analysis #





# ----------------------------------------------------------------------------------------------- #
                                  # MODULUS #

# MODULUS 10
def get_modulo_10(int)
	int.to_i % 10
end

# MODULUS 10
def get_modulo_11(int)
	int.to_i % 11
end

# ----------------------------------------------------------------------------------------------- #
                            # CHECK DIGIT VALIDATION FUNCTIONS #

# Get The actual check digit from the isbn
def get_check_digit(isbn)
	make_array(isbn)[-1].to_i
end

# Is THE LAST character an x or X?
def last_digit_is_x?(isbn)
	isbn.end_with?('X') || isbn.end_with?('x') 
end

# Is THIS character an x or X?
def is_x?(char)
	char == 'x' || char == 'X'
end

# Get the "theoretical" check digit
def calculated_check_digit(isbn)
	get_modulo_10(10 - get_modulo_10(get_sum_13(multiply_digits(isbn))))
end

# Does the calculated check digit match the actual last digit of the isbn
def check_digit_valid?(isbn)
	calculated_check_digit(isbn) == get_check_digit(isbn)
end

# ----------------------------------------------------------------------------------------------- #
                                  # FINAL VALIDATION #



# BOOLEAN ISBN-10 is valid (Bad way)
def final_validate_10(isbn)
	valid = true

	if valid_length(isbn) && validate_characters_10(isbn)
		without_spaces = without_spaces(isbn)
		without_dashes = without_dashes(without_spaces)
		without_dashes = without_dashes
		arry = make_array(without_dashes)
		last_digit = arry.pop
		last_digit = 10 if is_x?(last_digit) 
		m_by_index = multiply_by_index(arry)
		sum = get_sum(m_by_index)
		mod = get_modulo_11(sum)
		valid = false if mod != last_digit.to_i
	else
		valid = false
	end
	valid
end

# # BOOLEAN ISBN-13 is valid (Good way)
def final_validate_13(isbn)
	valid_length_13?(isbn) && validate_characters(isbn) && check_digit_valid?(isbn)
end

# Either 10 or 13 is valid
def valid?(isbn)
	length = valid_length(isbn)

	if length == 10
		final_validate_10(isbn)
	elsif length == 13
		final_validate_13(isbn)
	else
		false
	end

end

# ----------------------------------------------------------------------------------------------- #
                                      # CSV FUNCTIONS #
def write_csv(path, isbn)
	length = valid_length(isbn)

	if length == 10 || length == 13 
		CSV.open(path, 'a') do |line|
			line << ["\"#{isbn}\"", "\"#{length}\"", "\"#{valid?(isbn).to_s}\""]
		end
	end
end

def read_csv
	html = ''
	html << '<table>'
	html << "<thead>
		<tr>
		  <th scope=\"col\">ISBN</th>
		  <th scope=\"col\">Type</th>
		  <th scope=\"col\">Valid</th>
		</tr>
	</thead>"
	CSV.foreach('output.csv', 'r') do |line|
		html << '<tr>'
		line.each {|value| html << "<td>#{value}</td>"}
		html << '</tr>'
	end
	html << '</table>'
	html
end

def process_csv(path)
	# For each line in the uploaded file
	CSV.foreach(path, 'r') do |line|
		#for each value in the line... write the value to 
		line.each {|value| write_csv('processed_csv.csv', value)}
	end
end

# ----------------------------------------------------------------------------------------------- #
                                  # VALIDATION MESSAGES #

#
def get_validation_message_10(isbn)
	if final_validate_10(isbn) == true
		"\"#{isbn}\" <br> <i>is</i> a valid isbn-10."
	else
		"\"#{isbn}\" <br> <i>is not</i> a valid isbn-10."
	end
end

#
def get_validation_message_13(isbn)
	if final_validate_13(isbn)
		"\"#{isbn}\" <br> <i>is</i> a valid isbn-13."
	else
		"\"#{isbn}\" <br> <i>is not</i> a valid isbn-13."
	end
end

#
def get_validation_message(isbn)
	if valid_length(isbn) == 13
		get_validation_message_13(isbn).to_s
	elsif valid_length(isbn) == 10
		get_validation_message_10(isbn).to_s
	else
		"\"#{isbn}\" <br> is invalid."
	end
end
