AIM:
Create a program that will verify if a string is a valid ISBN number (see requirements below).
Use a TDD approach.
This is a big exercise - break it down into chunks!

REQUIREMENTS FOR ISBN
ISBN-10 is made up of 9 digits plus a check digit (which
may be 'X') and ISBN-13 is made up of 12 digits plus a
check digit. 

- Spaces and hyphens may be included in a code,
but are not significant. This means that 9780471486480 is
equivalent to 978-0-471-48648-0 and 978 0 471 48648 0.

- The check digit for ISBN-10 is calculated by multiplying
each digit by its position (i.e., 1 x 1st digit, 2 x 2nd
digit, etc.), summing these products together and taking
modulo 11 of the result (with 'X' being used if the result
is 10).

- The check digit for ISBN-13 is calculated by
  -- multiplying each digit alternately by 1 or 3
  -- summing these products together, 
  -- taking modulo 10 of the result
  -- subtracting this value from 10,
  -- then taking the modulo 10 of the result again to produce a single digit.

Examples of valid ISBN-13:
"9780470059029"
"978-0-13-149505-0"
"978 0 471 48648 0"
Examples of valid ISBN-10:
"0471958697"
"0-321-14653-0"
"877195869x"
Examples of invalid ISBNs:
"4780470059029"
"0-321@14653-0"
"877195x869"
""
" "
"-"
Example of how the ISBN-10 sumcheck is calculated:
first 9 digits of an isbn10: 742139476
create checksum:
sum = 1*7 + 2*4 + 3*2 + 4*1 + 5*3 + 6*9 7*4 + 8*7 + 9*6
sum = 7 + 8 + 6 + 4 + 15 + 54 + 28 + 56 + 54
sum = 232
checksum = 232%11
checksum = 1
isbn = 7421394761 

# -------------------------------------------------------------------------------------------------- #
                                      # 

Now to: 

   - write function for generating validation message(like in isbn 10 function), 

   - write a function that handles either isbn type using conditional (and then calls either validation 
     messsage generating function), 

   - combine functions from the separate files, 

   - combine tests from separate files, 

   - write functionality for csv io, 
   
   - make oop.

# -------------------------------------------------------------------------------------------------- #
C:\Users\Patrick\Desktop\projects\MINED_MINDS\isbn>irb
irb(main):001:0> ls
NameError: undefined local variable or method `ls' for main:Object
        from (irb):1
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):002:0> ls
NameError: undefined local variable or method `ls' for main:Object
        from (irb):2
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):003:0> ls
NameError: undefined local variable or method `ls' for main:Object
        from (irb):3
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):004:0> str = "0471958697"
=> "0471958697"
irb(main):005:0> str
=> "0471958697"
irb(main):006:0> str.match? /[0-9]/
NoMethodError: undefined method `match?' for "0471958697":String
Did you mean?  match
        from (irb):6
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):007:0> str.match?(/[0-9]/)
NoMethodError: undefined method `match?' for "0471958697":String
Did you mean?  match
        from (irb):7
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):008:0> str.match(/[0-9]/)
=> #<MatchData "0">
irb(main):009:0> str.match(/[0-9- ]/)
(irb):9: warning: character class has '-' without escape: /[0-9- ]/
(irb):9: warning: character class has '-' without escape: /[0-9- ]/
=> #<MatchData "0">
irb(main):010:0> str.match(/[0-9\- ]/)
=> #<MatchData "0">
irb(main):011:0> str.match(/[0-9\-\ ]/)
=> #<MatchData "0">
irb(main):012:0> /[0-9\-\ ]/.match? str
NoMethodError: undefined method `match?' for /[0-9\-\ ]/:Regexp
Did you mean?  match
        from (irb):12
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):013:0> ls
NameError: undefined local variable or method `ls' for main:Object
        from (irb):13
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):014:0> str
=> "0471958697"
irb(main):015:0> str.match?(/0-9/)
NoMethodError: undefined method `match?' for "0471958697":String
Did you mean?  match
        from (irb):15
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):016:0> str.match(/0-9/)
=> nil
irb(main):017:0> ls
NameError: undefined local variable or method `ls' for main:Object
        from (irb):17
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):018:0> str.match(/[0-9]/)
=> #<MatchData "0">
irb(main):019:0> str.match(/[0-9]?/)
=> #<MatchData "0">
irb(main):020:0> str.match(/?[0-9]/)
SyntaxError: (irb):20: target of repeat operator is not specified: /?[0-9]/
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):021:0> str.match(/[0-9]*/)
=> #<MatchData "0471958697">
irb(main):022:0> str.match(/[0-9]{3}/)
=> #<MatchData "047">
irb(main):023:0> str.match?(/[0-9]{3}/)
NoMethodError: undefined method `match?' for "0471958697":String
Did you mean?  match
        from (irb):23
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):024:0> str.match(/[0-9]{3}/)
=> #<MatchData "047">
irb(main):025:0> str.match(/[0-9]{9}/)
=> #<MatchData "047195869">
irb(main):026:0> str.match(/[0-9]{10}/)
=> #<MatchData "0471958697">
irb(main):027:0> str.include? /[0-9]{10}/
TypeError: no implicit conversion of Regexp into String
        from (irb):27:in `include?'
        from (irb):27
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):028:0> str.include? /[0-9]{10}/
TypeError: no implicit conversion of Regexp into String
        from (irb):28:in `include?'
        from (irb):28
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):029:0> str.match(/[0-9]{3}/)
=> #<MatchData "047">
irb(main):030:0> str.match(/[0-9- ]{3}/)
(irb):30: warning: character class has '-' without escape: /[0-9- ]{3}/
(irb):30: warning: character class has '-' without escape: /[0-9- ]{3}/
=> #<MatchData "047">
irb(main):031:0> str2 = "0-321-14653-0"
=> "0-321-14653-0"
irb(main):032:0> str2.match(/[0-9- ]{3}/)
(irb):32: warning: character class has '-' without escape: /[0-9- ]{3}/
(irb):32: warning: character class has '-' without escape: /[0-9- ]{3}/
=> #<MatchData "0-3">
irb(main):033:0> str2.match(/[0-9- ]{9}/)
(irb):33: warning: character class has '-' without escape: /[0-9- ]{9}/
(irb):33: warning: character class has '-' without escape: /[0-9- ]{9}/
=> #<MatchData "0-321-146">
irb(main):034:0> str2.delete(' ').delete('-').match(/[0-9]{9}/)
=> #<MatchData "032114653">
irb(main):035:0> str2.delete(' ').delete('-').match(/[0-9]{10}/)
=> #<MatchData "0321146530">
irb(main):036:0> str2.delete(' ').delete('-').match(/[0-9]{10}/).length
=> 1
irb(main):037:0> str2.delete(' ').delete('-').match(/[0-9]{10}/).length
=> 1
irb(main):038:0> str2.delete(' ').delete('-').match(/[0-9]{10}/).count
NoMethodError: undefined method `count' for #<MatchData "0321146530">
        from (irb):38
        from C:/RailsInstaller/Ruby2.3.0/bin/irb.cmd:19:in `<main>'
irb(main):039:0> str2.delete(' ').delete('-').match(/[0-9]{10}/).length^C
irb(main):039:0> ^C
irb(main):039:0> ^C
irb(main):039:0> quit
Terminate batch job (Y/N)? Y
