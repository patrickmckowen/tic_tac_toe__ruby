#.search.query
class Db
  
  def initialize(host, port, user, password, name)
    @host = host
    @port = port
    @user = user
    @password = password        # Fix Bcrypt module
    @name = name
    @tables = []                # Just adding but not giving option to give options on init
  end

  def host()
    @host
  end

  def port()
    @port
  end

  def name()
    @name
  end

  def user()
    @user
  end

  def password()
    @password # Fix Bcrypt module problem
  end

end
