require "minitest/autorun"
require_relative "User.rb"
require_relative "Db.rb"
load "./local_env_testing.rb" if File.exists?("./local_env_testing.rb")

class User_test < Minitest::Test

  # Test 1 - Get User's first name
  def test_get_user_first_name
    test1 = User.new('userSteve',
                 '234kjhvoaasd879g',
                 'Steve',
                 'Stevenson',
                 'Steve Street',
                 'Steveville',
                 'Steveahoma',
                 '12345',
                 '1-304-555-3045')
    goal = 'Steve'
    assert_equal(goal, test1.first_name)
  end

  # Test 2 - Get User's last name
  def test_get_user_last_name
    test2 = User.new('userSteve',
                 '234kjhvoaasd879g',
                 'Steve',
                 'Stevenson',
                 'Steve Street',
                 'Steveville',
                 'Steveahoma',
                 '12345',
                 '1-304-555-3045')
    goal = 'Stevenson'
    assert_equal(goal, test2.last_name)
  end

  # Test 3 - Get User's street address
  def test_get_user_street_address
    test3 = User.new('userSteve',
                 '234kjhvoaasd879g',
                 'Steve',
                 'Stevenson',
                 'Steve Street',
                 'Steveville',
                 'Steveahoma',
                 '12345',
                 '1-304-555-3045')
    goal = 'Steve Street'
    assert_equal(goal, test3.street_address)
  end

  # Test 4 - Get User's city
  def test_get_user_city
    test4 = User.new('userSteve',
                 '234kjhvoaasd879g',
                 'Steve',
                 'Stevenson',
                 'Steve Street',
                 'Steveville',
                 'Steveahoma',
                 '12345',
                 '1-304-555-3045')
    goal = 'Steveville'
    assert_equal(goal, test4.city)
  end

  # Test 5 - Get User's state
  def test_get_user_state
    test5 = User.new('userSteve',
                 '234kjhvoaasd879g',
                 'Steve',
                 'Stevenson',
                 'Steve Street',
                 'Steveville',
                 'Steveahoma',
                 '12345',
                 '1-304-555-3045')
    goal = 'Steveahoma'
    assert_equal(goal, test5.state)
  end

  # Test 6 - Get User's zipcode
  def test_get_user_zipcode
    test6 = User.new('userSteve',
                 '234kjhvoaasd879g',
                 'Steve',
                 'Stevenson',
                 'Steve Street',
                 'Steveville',
                 'Steveahoma',
                 '12345',
                 '1-304-555-3045')
    goal = '12345'
    assert_equal(goal, test6.zipcode)
  end

  # Test 7 - Get User's phone_number
  def test_get_user_phone
    test7 = User.new('userSteve',
                 '234kjhvoaasd879g',
                 'Steve',
                 'Stevenson',
                 'Steve Street',
                 'Steveville',
                 'Steveahoma',
                 '12345',
                 '1-304-555-3045')
    goal = '1-304-555-3045'
    assert_equal(goal, test7.phone)
  end

end


class Db_test < Minitest::Test

  def test_get_host
    test8 = Db.new(ENV['host'],
                   ENV['port'],
                   ENV['user'],
                   ENV['password'],
                   ENV['dbname'])
    goal = 'the host'
    assert_equal(goal, test8.host)
  end

  def test_get_port
    test9 = Db.new(ENV['host'],
                   ENV['port'],
                   ENV['user'],
                   ENV['password'],
                   ENV['dbname'])
    goal = 'the port'
    assert_equal(goal, test9.port)
  end

  def test_get_user
    test10 = Db.new(ENV['host'],
                   ENV['port'],
                   ENV['user'],
                   ENV['password'],
                   ENV['dbname'])
    goal = 'the user'
    assert_equal(goal, test10.user)
  end

  def test_get_password
    test11 = Db.new(ENV['host'],
                   ENV['port'],
                   ENV['user'],
                   ENV['password'],
                   ENV['dbname'])
    goal = 'the password'
    assert_equal(goal, test11.password)
  end

  def test_get_name
    test12 = Db.new(ENV['host'],
                   ENV['port'],
                   ENV['user'],
                   ENV['password'],
                   ENV['dbname'])
    goal = 'the database name'
    assert_equal(goal, test12.name)
  end

end
