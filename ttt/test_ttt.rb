require "minitest/autorun"
require_relative 'ttt.rb'

class Ttt_test < Minitest::Test

	# Instance variable of master class - valid_positions
	def test__ttt__valid_coords
		ttt= Ttt.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, ttt.get_valid_coords)
	end

####################################################################

	# Instance variable of master class - valid_responses
	def test__ttt__valid_choices
		ttt= Ttt.new()
		goal = ['1','2','3','4','5','6','7','8','9']
		assert_equal(goal, ttt.get_valid_choices)
	end

####################################################################


	# Instance variable of master class - valid_positions
	def test__ttt__remove_open_coords
		ttt= Ttt.new()
		ttt.remove_open_coords([1, 1])
		goal = [[1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, ttt.get_open_coords)
	end

####################################################################

	# Instance variable of master class - valid_responses
	def test__ttt__open_choices
		ttt= Ttt.new()
		ttt.remove_open_choice('1')
		goal = ['2','3','4','5','6','7','8','9']
		assert_equal(goal, ttt.get_open_choices)
	end

####################################################################

	# Instance variable of master class - winning combinations
	def test_3_ttt__winning_combinations
		ttt= Ttt.new()
		goal = [[[1, 1], [1, 5], [1, 9]],     # Horizantal
			   [[5, 1], [5, 5], [5, 9]],     # Horizantal
			   [[9, 1], [9, 5], [9, 9]],  # Horizantal
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 5], [5, 5], [9, 5]],     # Vertical
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 1], [5, 5], [9, 9]],    # Diagonal
			   [[9, 1], [5, 5], [1, 9]]]    # Diagonal
			   
		assert_equal(goal, ttt.get_winning_combinations)
	end
	
####################################################################

	# Instance variable of master class - winning combinations
	def test_3_ttt__array
		ttt= Ttt.new()
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
			   
		assert_equal(goal, ttt.get_array)
	end

end #grtpies
