require "minitest/autorun"
require_relative 'game.rb'
require_relative '../board/board.rb'
require_relative '../player/robot/sequential/sequential.rb'

class Game_test < Minitest::Test


	def test__setup_players
		print("Type 's' for player 1 and something else for p 2. \n\n")
		game = Game.new()
		#game.player_1 = Robot_sequential.new('Steve', 'O')
		#game.player_2 = Robot_sequential.new('Mike', 'X')
		game.setup_players()
		goal = 's'
		assert_equal(goal, game.active_player.name)
	end

	def test__active_player__without_switch
		#print("Type 's' for player 2 and something else for p 1. \n\n")
		game = Game.new()
		game.player_1 = Robot_sequential.new('Steve', 'O')
		game.player_2 = Robot_sequential.new('Mike', 'X')
		game.active_player = game.player_1
		game.inactive_player = game.player_2
		goal = 'Steve'
		assert_equal(goal, game.active_player.name)
	end


	def test__active_player__with_switch
		#print("Type 'm' \n\n")
		game = Game.new()
		game.player_1 = Robot_sequential.new('Steve', 'O')
		game.player_2 = Robot_sequential.new('Mike', 'X')
		game.active_player = game.player_1
		game.inactive_player = game.player_2
		game.switch_active_player
		goal = 'Mike'
		assert_equal(goal, game.active_player.name)
	end
	

end
