require_relative '../ttt.rb'
require_relative "../board/board.rb"
require_relative "../player/human/human.rb"
require_relative "../player/robot/sequential/sequential.rb"
require_relative "../player/robot/random/random.rb"
require_relative "../player/robot/unbeatable/unbeatable.rb"


class Game <Ttt
	
	attr_accessor :player_1, :player_2, :board, :turn, :winner, :active_player, :inactive_player
	
	def initialize()
		@board = Board.new()
		@player_1
		@player_2
		@active_player
		@inactive_player
		@turn 
		@winner = nil
	end
# --------------------------------------------------------------- #

	def setup_players_web_app(name1, type1, name2, type2)
		@player_1 = setup_player_1_web_app(name1, type1)
		@player_2 = setup_player_2_web_app(name2, type2)
		@active_player = @player_1
		@inactive_player = @player_2
		@turn = @active_player.marker
	end
	

	def setup_player_1_web_app(name, type)
		if type == 1
			@player_1 = Human.new(name, 'X')
		elsif type == 2
			@player_1 = Robot_sequential.new(name, 'X')
		elsif type == 3
			@player_1 = Robot_random.new(name, 'x')
		elsif type == 4
			@player_1 = Robot_unbeatable.new(name, 'x')
		else
			puts "Invalid Input"
			setup_player_1()
		end
	end
	
	def setup_player_2_web_app(name, type)
		if type == 1
			@player_2 = Human.new(name, 'O')
		elsif type == 2
			@player_2 = Robot_sequential.new(name, 'O')
		elsif type == 3
			@player_2 = Robot_random.new(name, 'O')
		elsif type == 4
			@player_2 = Robot_unbeatable.new(name, 'O')
		else
			puts "Input was not a number between 1-3..."
			setup_player_2()
		end
	end
# --------------------------------------------------------------- #
	
	def intro_prompt()
		"""
Welcome!
Let's first set the players up...
		
		"""
	end

	def setup_players()
		@player_1 = setup_player_1()
		@player_2 = setup_player_2()
		@active_player = @player_1
		@inactive_player = @player_2
		@turn = @active_player.marker
	end
	

	def setup_player_1()
		puts """
Please Select a Player1's Type from the list below
1 - Human
2 - Easy Computer (Sequential Computer)
3 - Medium Computer (Random Computer)
4 - Unbeatable
			"""
		type = gets.chomp.to_i


		puts """
What is this player's name?
			"""
		name = gets.chomp
		

		if type == 1
			@player_1 = Human.new(name, 'X')
		elsif type == 2
			@player_1 = Robot_sequential.new(name, 'X')
		elsif type == 3
			@player_1 = Robot_random.new(name, 'x')
		elsif type == 4
			@player_1 = Robot_unbeatable.new(name, 'x')
		else
			puts "Invalid Input"
			setup_player_1()
		end
	end
	
	def setup_player_2()
		puts """
Please Select a Player2's Type from the list below
1 - Human
2 - Easy Computer (Sequential Computer)
3 - Medium Computer (Random Computer)
4 - Unbeatable
			"""
		type = gets.chomp.to_i

		
		puts """
What is this player's name?
			"""
		name = gets.chomp
		
		
		if type == 1
			@player_2 = Human.new(name, 'O')
		elsif type == 2
			@player_2 = Robot_sequential.new(name, 'O')
		elsif type == 3
			@player_2 = Robot_random.new(name, 'O')
		elsif type == 4
			@player_2 = Robot_unbeatable.new(name, 'O')
		else
			puts "Input was not a number between 1-3..."
			setup_player_2()
		end
	end
	
	def switch_active_player()
		temp_active_player = @active_player
		temp_inactive_player = @inactive_player
		@active_player = temp_inactive_player
		@inactive_player = temp_active_player
	end
	
	def ending_prompt()
		if @winner == nil                                       # If there's not a winner
			print("\n", "Cat's game!")                         # Tell its a cats game
		else                                                   # 
			print("#{@active_player.name} is the winner!")   # Congradualte
		end
	end

	def ending_prompt_web_app()
		if @winner == nil                                       # If there's not a winner
			return 'Cat\'s game!'                         # Tell its a cats game
		else                                                   # 
			return "#{@active_player.name} is the winner!"   # Congradualte
		end
	end
	
	def handle_player_choice()
		choice = @active_player.choice(@board.get_array, @board.get_open_choices)
		puts("Player #{@active_player.marker} chose", choice)
		puts("This is found at #{@board.choice_to_coordinates(choice)}\n\n")
		#print()
		@board.handle_choice(@active_player.marker, choice)
	end
	
	def handle_player_choice_web_app()
		choice = @active_player.choice(@board.get_array, @board.get_open_choices)
		@board.handle_choice(@active_player.marker, choice)
	end
	
	def start()

		# INTRO
		intro_prompt()
		
		# Setup players
		setup_players()
		
		# Display the board
		@board.display()
      
		# MAIN LOOP
		until @winner != nil || @board.full?
		
			# Swith turns if applicable
			switch_active_player() #if @winner == nil || @board.full? == false                                       
			
			# Tell players who's turn it is
			print("It is #{@active_player.name}'s turn. \n")
			
			# Get the players choice
			handle_player_choice()
			
			# Display the board
			@board.display()

			# Check for winner
			@winner = @board.winner()
      end
	  
	  # Either congradulates or tells players its a cats game
	  ending_prompt()
	end
	
end

=begin
class Game < Ttt
	
	attr_accessor :player_1, :player_2, :board, :winner, :active_player, :inactive_player,
	
	def initialize()
		super()
		@player_1 = ''#setup_player_1()
		@player_2 = ''#setup_player_2()
		@board = Board.new()
		            
		@winner = nil
		@active_player = @player_1
		@inactive_player = @player_2
	end
	
	
	# ------------------------------------------------------------------------------------------------------------ #
	
	
	def intro_prompt()
		"""
Welcome!
Let's first set the players up...
		
		"""
	end
	
	# ------------------------------------------------------------------------------------------------------------ #
	def setup_player_1()
		puts """
Please Select a Player 1 Type from the list below
1 - Human
2 - Easy Computer (Sequential Computer)
3 - Medium Computer (Random Computer)
4 - Unbeatable
			"""
			
		type = gets.chomp.to_i
		
		puts """
What is this player's name?
			"""
		name = gets.chomp.to_i

		if type == 1
			@player_1 = Human.new(name, 'X')
		elsif type == 2
			@player_1 = Robot_sequential.new(name, 'X')
		elsif type == 3
			@player_1 = Robot_random.new(name, 'X')
		elsif type == 4
			@player_2 = Robot_unbeatable.new(name, 'X')
		else
			puts "Invalid Input. Please pick a number between 1 - 4"
			setup_player_1()
		end
	end
	
	def setup_player_2()
		puts """
Please Select a Player 2's Type from the list below
1 - Human
2 - Easy Computer (Sequential Computer)
3 - Medium Computer (Random Computer)
4 - Unbeatable
			"""
		type = gets.chomp.to_i

		puts """
What is this player's name?
			"""
		name = gets.chomp.to_i

		if type == 1
			@player_1 = Human.new(name, 'O')
		elsif type == 2
			@player_1 = Robot_sequential.new(name, 'O')
		elsif type == 3
			@player_1 = Robot_random.new(name, 'O')
		elsif type == 4
			@player_2 = Robot_unbeatable.new(name, 'O')
		else
			puts "Input was not a number between 1-4..."
			setup_player_2()
		end
	end
	
	def switch_active_player                    
		temp_active_player = @active_player
		temp_inactive_player = @inactive_player
		@active_player = temp_inactive_player
		@inactive_player = temp_active_player
	end
	
	def ending_prompt()
		if @winner != nil                                       # If there's a winner
			print("#{@active_player.name} is the winner!")   # Congradualte
		else                                                   # 
			print("\n", "Cat's game!")                         # Tell its a cats game
		end
	end

	#
	def handle_player_choice()
		choice = @active_player.choice(@board.array, @board.open_choices)
		puts("#{@active_player.name} #{@active_player.marker} chose #{choice}")
		@board.handle_choice(@active_player.marker, choice)
	end
	
	#
	def start()

		# INTRO
		intro_prompt()
		setup_player_1
		setup_player_2
		
		# Display the board
		@board.display()
      
		# MAIN LOOP
		until @winner != nil || @board.full?

			# Swith turns if applicable
			switch_active_player()                                    
			
			# Tell players who's turn it is
			print("It is #{@active_player.name}'s turn. \n")
			
			# Get the players choice
			handle_player_choice()
			
			# Display the board
			@board.display()

			# Check for winner
			@winner = @board.winner()
		end
	  
	# Either congradulates or tells players its a cats game
	ending_prompt()
	  
	end
	
end


print game.active_player.marker

game.switch_active_player

print game.active_player.marker

print game.board.full?

game.board.place_marker(game.active_player.marker, 2)

game.board.display

game.active_player.choice


game = Game.new()
game.start


game = Game.new()
game.setup_players()

print "active player.marker: ", game.active_player.marker, "\n\n"

game.switch_active_player

print "active player.marker after switch: ", game.active_player.marker, "\n\n"

print game.board.full?

#game.board.mark(game.active_player.marker, '2')
print "active player.name after switch: ", game.active_player.name, "\n\n"
game.board.display

#game.active_player.choice
=end
#Game.new().start()
