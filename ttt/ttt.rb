# Main Class for tic tac toe assignment

class Ttt
	attr_reader :array, :valid_positions, :valid_responses, :winning_combinations
	
	# The playsurface itself
	@@array = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
	
	
	#
	@@valid_coords = [[1, 1], [1, 5], [1, 9], 
				      [5, 1], [5, 5], [5, 9], 
				      [9, 1], [9, 5], [9, 9]]
	
	#
	@@valid_choices = ['1','2','3',
					   '4','5','6',
					   '7','8','9']
					   
			   
	# 
	@@open_coords = [[1, 1], [1, 5], [1, 9], 
					 [5, 1], [5, 5], [5, 9], 
					 [9, 1], [9, 5], [9, 9]]
					    
	#
	@@open_choices = ['1', '2', '3',
					  '4', '5', '6',
					  '7', '8', '9']
	
	#
	@@winning_combinations = [[[1, 1], [1, 5], [1, 9]],     # Horizantal
							  [[5, 1], [5, 5], [5, 9]],     # Horizantal
							  [[9, 1], [9, 5], [9, 9]],     # Horizantal
							  [[1, 1], [5, 1], [9, 1]],     # Vertical
							  [[1, 5], [5, 5], [9, 5]],     # Vertical
							  [[1, 1], [5, 1], [9, 1]],     # Vertical
							  [[1, 1], [5, 5], [9, 9]],     # Diagonal
							  [[9, 1], [5, 5], [1, 9]]]     # Diagonal
							  
=begin
	@@winning_combinations = [[1, 2, 3],     # Horizantal
							  [4, 5, 6],     # Horizantal
							  [7, 8, 9],     # Horizantal
							  [1, 4, 7],     # Vertical
							  [[1, 5], [5, 5], [9, 5]],     # Vertical
							  [[1, 1], [5, 1], [9, 1]],     # Vertical
							  [[1, 1], [5, 5], [9, 9]],     # Diagonal
							  [[9, 1], [5, 5], [1, 9]]]     # Diagonal
=end


	# ------------------------------------------------------------ #
	# Accessors 
	
	def get_array()
		@@array
	end
	
	def get_valid_coords()
		@@valid_coords
	end
	
	def get_valid_choices()
		@@valid_choices
	end
	
	def get_open_coords()
		@@open_coords
	end
	
	def get_open_choices()
		@@open_choices
	end
	
	def get_winning_combinations()
		@@winning_combinations
	end
	
	def reset
		@@array = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			   [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
			# 
		@@open_coords = [[1, 1], [1, 5], [1, 9], 
						 [5, 1], [5, 5], [5, 9], 
						 [9, 1], [9, 5], [9, 9]]
					    
	#
		@@open_choices = ['1', '2', '3',
						  '4', '5', '6',
						  '7', '8', '9']
	end

	# ------------------------------------------------------------ #
	# Assemble a list of the 9 positions on the current board
	def condensed_board_array()
		tmp = []
		@@valid_coords.each {|p| tmp << @@array[p[0]][p[1]]}
		tmp
	end
	
	# Convert choice to coordinates for the board
	def choice_to_coordinates(choice)                           
		lookup = {'1' => [1, 1], '2' => [1, 5], '3' => [1, 9],
				  '4' => [5, 1], '5' => [5, 5], '6' => [5, 9],
				  '7' => [9, 1], '8' => [9, 5], '9' => [9, 9]}
		coordinates = lookup[choice]
		coordinates
	end
	
	# Convert board coordinates to player choice
	def coordinates_to_choice(coordinates)
		lookup = {[1 , 1] => '1', [1, 5] => '2', [1, 9] => '3',
                  [5, 1] => '4', [5, 5] => '5',  [5, 9]=> '6',
                  [9, 1] => '7', [9, 5] => '8', [9, 9] => '9'}
		choice = lookup[coordinates]
		choice
	end
	
		#
	def marker_at(coords)
		get_array[coords[0]][coords[1]]
	end
	
	# ------------------------------------------------------------ #
	def remove_open_coords(coords)
		@@open_coords.delete(coords)
	end
	
	def add_open_coords(coords)
		@@open_coords << coords
	end
	
	def remove_open_choice(choice)
		@@open_choices.delete(choice)
	end
	
	def add_open_choice(choice)
		@@open_choices << choice
	end
	# ------------------------------------------------------------ #

	def insert_marker(marker, coords)
		@@array[coords[0]][coords[1]] = marker
	end
	# ------------------------------------------------------------ #
end
