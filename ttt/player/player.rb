require_relative '../ttt.rb'

class Player < Ttt

	attr_accessor :name, :marker, :opponent_marker
	
	#
	def initialize(name, marker)
		super()
		@name = name
		@marker = marker
		@opponent_marker = get_opponent_marker()
	end
	
	#
	def get_opponent_marker()
		marker = ''
		if @marker == 'X'
			marker = 'O'
		else
			marker = 'X'
		end
		marker
	end
	
	# Return the winning combination array but with the characters in place of coordinates where there are characters
	def examine_winning_combinations(board_array)
		temp = []
		get_winning_combinations.each do |combination|
			temp_arry = []
			combination.each do |coords|
				if board_array[coords[0]][coords[1]] != self.marker && board_array[coords[0]][coords[1]] != ' '
					temp_arry << board_array[coords[0]][coords[1]]
				elsif board_array[coords[0]][coords[1]] == self.marker
					temp_arry << self.marker
				else board_array[coords[0]][coords[1]] == ' '
					temp_arry << coords
				end
			end
			temp << temp_arry
		end
		temp
	end
	
	#Looks at the current status of the board (at the beggining of turn) and returns true of false
	def went_first?(board_array)
		tmp = []
		@@array.each {|i| i.each {|x| tmp << x if x != ' ' && x != '#'}}
		true ? tmp.count(@marker) == tmp.count(@opponent_marker) : false
	end
	
	# 
	def turn_num(board_array)
		tmp = []
		@@array.each {|i| i.each {|x| tmp << x if x != ' ' && x != '#'}}
		tmp.length + 1
	end
	
end
