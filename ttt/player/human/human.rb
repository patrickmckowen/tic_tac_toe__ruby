require_relative '../player.rb'

class Human < Player
	attr_accessor :name, :marker

	def choice(board, open_choices)
		response = nil
		until get_valid_choices.include?(response) && get_open_choices.include?(response)
			print("\n\nPlease choose a number between 1 and 9.\n")
			response = gets.chomp.to_s
			if get_valid_choices.include?(response)
				if not get_open_choices.include?(response)
					print("\n\nThis position has been chosen...** \n")
				end
			else
				print("\n\n\"#{response}\" is an invalid choice. Please choose a number from 1-9.** \n")
			end
		end
		response
	end
end
