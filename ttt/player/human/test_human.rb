require "minitest/autorun"

####################################################################

require_relative 'human.rb'
require_relative '../../board/board.rb'


########################################################################################################################################

class Human_test < Minitest::Test
	
	def test__opponent_marker
		human = Human.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, human.opponent_marker)
	end

####################################################################
	def test__get_valid_coords
		human = Human.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, human.get_valid_coords)
	end
	
	def test__get_valid_choices
		human = Human.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, human.get_valid_choices)
	end
	
	def test__name
		human = Human.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, human.name)
	end
	
	def test__marker
		human = Human.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, human.marker)
	end
	
	def test__get_array
		human = Human.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, human.get_array)
	end
	
	def test__get_opponent_marker
		human = Human.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, human.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		human = Human.new('Steve', 'X')
		goal = true
		assert_equal(goal, human.went_first?(board.array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		human = Human.new('Steve', 'X')
		goal = false
		assert_equal(goal, human.went_first?(board.array))
		board.mark(' ', [1, 1])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		human = Human.new('Steve', 'X')
		goal = 3
		assert_equal(goal, human.turn_num(board.array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end


####################################################################

	def test__human__choice__pick_1
		human = Human.new('Steve', 'X')
		board = Board.new()
		goal = '1'
		print("Choose 1 \n \n")
		assert_equal(goal, human.choice(board.array, board.get_open_choices))
	end

####################################################################

	def test__human__choice__pick_2
		human = Human.new('Steve', 'X')
		board = Board.new()
		goal = '2'
		print("Choose 2 \n\n")
		assert_equal(goal, human.choice(board.array, board.get_open_choices))
	end

####################################################################

	def test__human__choice__pick_6
		human = Human.new('Steve', 'X')
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		board.handle_choice('X', '9')
		goal = '6'
		print("Choose 9, then 8, then, 7, then 6 \n\n")
		assert_equal(goal, human.choice(board.array, board.get_open_choices))
		board.mark(' ', [9, 1])
		board.mark(' ', [9, 5])
		board.mark(' ', [9, 9])
		board.add_open_coords([9, 1]) # put it back
		board.add_open_coords([9, 5]) # put it back
		board.add_open_coords([9, 9]) # put it back
		board.add_open_choice('7') # put it back
		board.add_open_choice('8') # put it back
		board.add_open_choice('9') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end

end
