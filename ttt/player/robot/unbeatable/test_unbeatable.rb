require "minitest/autorun"
require_relative 'unbeatable.rb'
require_relative '../../../board/board.rb'

class Game_test < Minitest::Test

	def test__unbeatable__get_valid_coords
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, unbeatable.get_valid_coords)
	end
	
	def test__get_valid_choices
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, unbeatable.get_valid_choices)
	end
	
	def test__name
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, unbeatable.name)
	end
	
	def test__marker
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, unbeatable.marker)
	end
	
	def test__get_array
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, unbeatable.get_array)
	end
	
	def test__get_opponent_marker
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, unbeatable.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = true
		assert_equal(goal, unbeatable.went_first?(board.get_array))
		board.reset()
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = false
		assert_equal(goal, unbeatable.went_first?(board.array))
		board.reset()
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 3
		assert_equal(goal, unbeatable.turn_num(board.array))
		board.reset()
	end
	
####################################################################						
						
	def test__unbeatable__examine_winning_combinations__1
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('O', '2')
		board.handle_choice('O', '9')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [["O", "O", [1, 9]], [[5, 1], [5, 5], [5, 9]], [[9, 1], [9, 5], "O"], ["O", [5, 1], [9, 1]], ["O", [5, 5], [9, 5]], ["O", [5, 1], [9, 1]], ["O", [5, 5], "O"], [[9, 1], [5, 5], [1, 9]]]
		assert_equal(goal, unbeatable.examine_winning_combinations(board.get_array))
		board.reset()
	end
	
####################################################################

	def test__detect_two_in_a_row__o
		board = Board.new()
		board.handle_choice('O', '4')
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = ['O', [5, 9]]
		assert_equal(goal, unbeatable.detect_2_in_a_row(board.get_array))
		board.reset()
	end

####################################################################

	def test__detect_two_in_a_row__x
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = ['X', [5, 9]]
		assert_equal(goal, unbeatable.detect_2_in_a_row(board.get_array))
		board.reset()
	end


####################################################################

	# BLOCK MIDDLE ROW MIDDLE COLUMN
	def test_unbeatable__block__middle_row
		board = Board.new()
		board.handle_choice('O', '4')
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '6'
		assert_equal(goal, unbeatable.block__choice(board.get_array))
		board.reset()
	end

####################################################################

	# BLOCK BOTTOM ROW
	def test_unbeatable__block__bottom_row
		board = Board.new()
		board.handle_choice('O', '7')
		board.handle_choice('O', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.block__choice(board.get_array))
		board.reset()
	end

####################################################################

	# BLOCK DIAGONAL AT BOTTOM RIGHT
	def test_unbeatable__block__diagonal
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.block__choice(board.get_array))
		board.reset()
	end


####################################################################

	# 
	def test_unbeatable__win__middle_row
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '6'
		assert_equal(goal, unbeatable.win__choice(board.get_array))
		board.reset()
	end

####################################################################

	# WIN BOTTOM ROW
	def test_unbeatable__win__bottom_row
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.win__choice(board.get_array))
		board.reset()
	end

####################################################################

	# WIN DIAGONAL AT BOTTOM RIGHT
	def test_unbeatable__win__diagonal
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.win__choice(board.get_array))
		board.reset()
	end

####################################################################

	# CSV TURN MATCH
	def test__unbeatable__csv__match_turn__1
		board = Board.new()
		board.handle_choice('X', '1')                                # Player went first = true
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')              # Player marker = X
		goal = [["O", "X", "X", "O", " ", "X", "X", " ", " ", "1"], [" ", " ", " ", "X", " ", " ", " ", " ", "O", "1"]]
		assert_equal(goal, unbeatable.csv__match_turn(board.get_array))
		board.reset()
	end

####################################################################

	# # CSV TURN MATCH
	def test__unbeatable__csv__match_turn__2
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('X', '5')                                #Player went first = false
		board.handle_choice('O', '6')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [["O", " ", " ", " ", "X", " ", " ", " ", "O", "2"], [" ", " ", "O", " ", "X", " ", " ", " ", " ", "5"], [" ", " ", " ", " ", "X", " ", "O", " ", " ", "5"], [" ", " ", " ", " ", "X", " ", " ", " ", "O", "5"], ["O", "X", "X", "O", " ", "X", "X", " ", " ", "1"], [" ", " ", "X", "O", " ", "X", "X", " ", " ", "1"]]
		assert_equal(goal, unbeatable.csv__match_turn(board.get_array))
		board.reset()
	end
	
####################################################################

	# # CSV board MATCH
	def test__board__csv__match_board__1
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('O', '9')                               
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [" ", " ", " ", "X", " ", " ", " ", " ", "O", "1"]
		assert_equal(goal, unbeatable.csv__match_board(board.get_array))
		board.reset()
	end

####################################################################	

	# CSV choice
	# Selects the final digit from the return of csv__board_match
	def test__unbeatable__csv__choice__1
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('O', '9')                             
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

########################################################################################################################################	
	
	# CSV choice
	# Selects the final digit from the return of csv__board_match
	def test__unbeatable__csv__choice__2nd_move__top_left
		board = Board.new()
		board.handle_choice('O', '1')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__unbeatable__csv__choice__2nd_move_top_right
		board = Board.new()
		board.handle_choice('X', '3')
		unbeatable = Robot_unbeatable.new('Steve', 'O')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__unbeatable__csv__choice__2nd_move__bottom_left
		board = Board.new()
		board.handle_choice('X', '7')
		unbeatable = Robot_unbeatable.new('Steve', 'O')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__unbeatable__csv__choice__2nd_move__bottom_right
		board = Board.new()
		board.handle_choice('X', '9')
		unbeatable = Robot_unbeatable.new('Steve', 'O')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__choice__block__1
		board = Board.new()
		board.handle_choice('O', '7')
		board.handle_choice('O', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__block__2
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('X', '8')
		board.handle_choice('O', '2')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '3'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__block__3
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('X', '8')
		board.handle_choice('O', '2')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '3'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__win__1
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__win__2
		board = Board.new()
		board.handle_choice('X', '2')
		board.handle_choice('O', '8')
		board.handle_choice('X', '3')
		board.handle_choice('O', '6')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# Handling forks

	def test__choice__fork__1
		board = Board.new()
		board.handle_choice('X', '2')
		board.handle_choice('O', '8')
		board.handle_choice('X', '3')
		board.handle_choice('O', '6')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
end
####################################################################

