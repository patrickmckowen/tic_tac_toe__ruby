require 'csv'
require_relative '../../player.rb'
require_relative '../../../board/board.rb'

class Robot_unbeatable < Player
	attr_accessor :winning_combinations, :board, :valid_positions
	
	# This one returns only the rows where the theoretical games match in
	## - Whether or not the robot went first
	## - WHat the current turn number is 
	## - Whether the player is the correct marker
	def csv__match_turn(board_array)
		tmp = []
		CSV.foreach('./reference.csv', 'r') do |line|
			if line[0].to_s == went_first?(board_array).to_s
				if line[1].to_i == turn_num(board_array)
					tmp << line[3..13] if line[2] == @marker
				end
			end
		end
		tmp
	end
	
	#
	def csv__match_board(board_array)
		board_match = nil                         # get the board conditions from csv line
		csv__match_turn(board_array).each {|t| board_match = t if t[0..8] == condensed_board_array()}
		board_match
	end
	
	#
	def csv__choice(board_array)
		csv__match_board(board_array).last if csv__match_board(board_array) != nil
	end
	
	# Returns the one that is two in a row, if there is one, otherwise nil
	def detect_2_in_a_row(board_array)
		two_in_a_row = []
		examine_winning_combinations(board_array).each do |combination|
			if combination.count('X') == 2 &&  combination.include?('O') == false
				two_in_a_row << 'X'
				combination.delete('X')
				two_in_a_row << combination[0]
			elsif combination.count('O') == 2 &&  combination.include?('X') == false
				two_in_a_row << 'O'
				combination.delete('O')
				two_in_a_row << combination[0]
			end
		end
		two_in_a_row
	end
	
	def block__choice(board_array)
		examined_array = examine_winning_combinations(board_array)
		#print(examined_array)
		blocking_spot = nil
		examined_array.each_with_index do |combination, index|
			if combination.count(@opponent_marker) == 2 &&  combination.include?(@marker) == false
				coords = ''
				combination.each_with_index {|item, index| coords = index if item.class == Array}
				blocking_spot = get_winning_combinations[index][coords]
				break
			end
		end
		coordinates_to_choice(blocking_spot)
	end
	
	
	#
	def fork__choice(board_array)
	
	end
	
	
	#
	def win__choice(board_array)
		examined_array = examine_winning_combinations(board_array)
		#print(examined_array)
		winning_spot = nil
		examined_array.each_with_index do |combination, index|
			#print('inside each loop', combination, "\n")
			if combination.count(@marker) == 2 &&  combination.include?(@opponent_marker) == false
				#print('inside if loop', combination)
				coords = ''
				combination.each_with_index {|item, index| coords = index if item.class == Array}
				winning_spot = get_winning_combinations[index][coords]
				break
			end
		end
		coordinates_to_choice(winning_spot)
	end


=begin
	#
	def block__choice(board_array)
		examined_array = examine_winning_combinations(board_array)
		blocking_spot = nil
		two_in_row_arry = detect_2_in_a_row(board_array)
		blocking_spot = two_in_row_arry[1] if two_in_row_arry[0] == @opponent_marker
		coordinates_to_choice(blocking_spot)
	end
	
	#
	def fork__choice(board_array)
	
	end
	
	#
	def win__choice(board_array)
		examined_array = examine_winning_combinations(board_array)
		blocking_spot = nil
		two_in_row_arry = detect_2_in_a_row(board_array)
		blocking_spot = two_in_row_arry[1] if two_in_row_arry[0] == @marker
		coordinates_to_choice(blocking_spot)
	end
=end
	#
	def choice(board_array, open_spaces)
		choice = nil
		choice = csv__choice(board_array)                    # If it's not in the file, the value will still be nil
		choice = block__choice(board_array) if choice == nil # If there an impending win by opponent, block it.
		choice = win__choice(board_array) if choice == nil   # If the player has a winning choice

		#choice = fork__choice(board_array) if choice == nil  # If there is not a win, check for a fork first
		choice = open_spaces.sample if choice == nil         # *Catch all* If still nill pick a random open space                 
		choice
	end
	
end

# -------------------------------------------------------------------------- #

#board = Board.new()
#board.handle_choice('O', '1')
#board.handle_choice('O', '2')
#board.handle_choice('X', '4')
#board.handle_choice('O', '9')
						
#robot = Robot_unbeatable.new('Steve', 'X')


# -------------------------------------------------------------------------- #
                       # INHERITED ATTR VARS # 
=begin
board = Board.new()
board.handle_choice('X', '4')
board.handle_choice('O', '9')
unbeatable = Robot_unbeatable.new('Steve', 'X')

print unbeatable.csv__match_board(board.array)
		   
#print(robot.name)
#print(robot.marker)
#print(robot.opponent_marker)	
#print(robot.valid_positions)			   
					   
#
#print(robot.went_first?(board.array))

#
#robot.csv__match_turn(board.array)


#
#print robot.csv__match_turn(board.array)
#print robot.condensed_board_array(board.array)
########################################################
		board = Board.new()
		board.handle_choice('O', '4')
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 'O'
		print(unbeatable.detect_2_in_a_row(board.get_array))


		board = Board.new()
		board.handle_choice('O', '7')
		board.handle_choice('O', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		print("\n\n\n", unbeatable.block__choice(board.get_array))
=end
