require "minitest/autorun"
require_relative 'random.rb'
require_relative '../../../board/board.rb'

class Random_test < Minitest::Test

	def test__random__get_valid_coords
		random = Robot_random.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, random.get_valid_coords)
	end
	
	def test__get_valid_choices
		random = Robot_random.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, random.get_valid_choices)
	end
	
	def test__name
		random = Robot_random.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, random.name)
	end
	
	def test__marker
		random = Robot_random.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, random.marker)
	end
	
	def test__get_array
		random = Robot_random.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, random.get_array)
	end
	
	def test__get_opponent_marker
		random = Robot_random.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, random.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		random = Robot_random.new('Steve', 'X')
		goal = true
		assert_equal(goal, random.went_first?(board.get_array))
		board.reset()
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		random = Robot_random.new('Steve', 'X')
		goal = false
		board.reset()
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		random = Robot_random.new('Steve', 'X')
		goal = 3
		assert_equal(goal, random.turn_num(board.array))
		board.reset()
	end
	
	def test__choice__1
		robot = Robot_random.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		board.handle_choice('X', '9')
		goal = '1'
		assert_equal(goal, robot.choice(board.array, board.open_choices))
		board.reset()
	end

####################################################################

	def test__choice__9
		robot = Robot_random.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		goal = '9'
		assert_equal(goal, robot.choice(board.array, board.open_choices))
		board.reset()
	end
	
####################################################################
	
	def test__choice__false
		robot = Robot_random.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '9')
		goal = '8'
		assert_equal(goal, robot.choice(board.array, board.open_choices))
		board.reset()
		
	end
	
end
