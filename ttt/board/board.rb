 require_relative '../ttt.rb'

class Board < Ttt
  attr_accessor :array, :winning_combinations, :valid_coords, :open_positions, :open_choices
	
	def display()
		final_string = "\n\n"
		get_array.each do |arry|
			arry.each {|value| final_string << value}
			final_string << "\n"
		end
		final_string << "\n\n"
		print(final_string)
		final_string
	end
	
	# Calls the previous function to remove the character, then places it in
	def mark(marker, coords)
		insert_marker(marker, coords)
		remove_open_choice(coordinates_to_choice(coords))
		remove_open_coords(coords)
	end
	
	def position_empty?(coords)
		marker_at(coords) == ' '
	end
	
	# Have all items from the @@available_positions array is empty
	def full?()
		@@open_coords.empty?
	end

	# First step in determining a win. Collects array of variable number of coordinates.
	def collect_characters(*coordinates)
		characters = []
		coordinates.each do |arry|
			characters << marker_at([arry[0], arry[1]])
		end
		characters
	end
	
	# Determines if the collected arrays are all the same character
	def same_characters?(arry)
		match = arry.all? {|x| x == arry[0]}
		match
	end
	
	#
	def winner()
		winner = nil
		get_winning_combinations.each do |comb|
		chars = collect_characters(comb[0], comb[1], comb[2])
			match = same_characters?(chars)
			match = false if chars[0] == " "                                        # Added conditional to account for matching empty spaces
			if match == true
				winner = chars[0]
				break
			end
		end
		winner
	end
	
	#
	def handle_choice(marker, choice)
		mark(marker, choice_to_coordinates(choice))
	end
	
	#

end
	
