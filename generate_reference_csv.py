#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 21 11:16:22 2017

@author: patrick

@purpose: This file contains the functions used to generate the reference csv.

@Explanation: This really only needs to cover the conditions to avoid a fork.
"""

#%%
import os
os.chdir(r"/home/patrick/Projects/ttt_rb/tic_tac_toe__ruby")
#%% Function: generate board condittions for X
def gen_board_cond_x(os, xs, pos):
    '''The first argument is a list of o's positions, 
    the second is a list of x's positions,
    the final arg is the position that should be chosen.'''
    lst = [' ', ' ', ' ',
           ' ', ' ', ' ',
           ' ', ' ', ' ']
    for i in os:
        lst[i-1] = 'O'
    for i in xs:
        lst[i-1] = 'X'
    turn_num = lst.count('X') + lst.count('O') + 1
    return "false,{},X,{},{},{},{},{},{},{},{},{},{}\n".format(turn_num,lst[0], lst[1], lst[2], lst[3], lst[4], lst[5], lst[6], lst[7], lst[8], pos)
#%% Function: generate board condittions for O
def gen_board_cond_o(xs, os, pos):
    '''The first argument is a list of o's positions, 
    the second is a list of x's positions,
    the final arg is the position that should be chosen.'''
    lst = [' ', ' ', ' ',
           ' ', ' ', ' ',
           ' ', ' ', ' ']
    for i in os:
        lst[i-1] = 'O'
    for i in xs:
        lst[i-1] = 'X'
    turn_num = lst.count('X') + lst.count('O') + 1
    return "false,{},O,{},{},{},{},{},{},{},{},{},{}\n".format(turn_num,lst[0], lst[1], lst[2], lst[3], lst[4], lst[5], lst[6], lst[7], lst[8], pos)
#%% Function: writeout to file
    
def writeout_second(arg_lists):
    with open('reference_generated.csv', 'a') as file:
        for i in arg_lists:
            file.write(gen_board_cond_x(i[0], i[1], i[2]))
            file.write(gen_board_cond_o(i[0], i[1], i[2]))
            
#%% DATA for going second
conditions_for_going_second = [
        [[1],[],5], # Take center if they took a corner
        [[3],[],5],
        [[7],[],5],
        [[9],[],5],
        
        [[5],[],1],     # If they took center spot, take 1
        
        [[1,9],[5],2],  # If they take opposite corners
        [[3,7],[5],2]]

#%% Writeout

writeout_second(conditions_for_going_second)

#%% Function: generate board condittions for X
def gen_board_cond_x_first(os, xs, pos):
    '''The first argument is a list of o's positions, 
    the second is a list of x's positions,
    the final arg is the position that should be chosen.'''
    lst = [' ', ' ', ' ',
           ' ', ' ', ' ',
           ' ', ' ', ' ']
    for i in os:
        lst[i-1] = 'O'
    for i in xs:
        lst[i-1] = 'X'
    turn_num = lst.count('X') + lst.count('O') + 1
    return "true,{},X,{},{},{},{},{},{},{},{},{},{}\n".format(turn_num,lst[0], lst[1], lst[2], lst[3], lst[4], lst[5], lst[6], lst[7], lst[8], pos)
#%% Function: generate board condittions for O
def gen_board_cond_o_first(xs, os, pos):
    '''The first argument is a list of o's positions, 
    the second is a list of x's positions,
    the final arg is the position that should be chosen.'''
    lst = [' ', ' ', ' ',
           ' ', ' ', ' ',
           ' ', ' ', ' ']
    for i in os:
        lst[i-1] = 'O'
    for i in xs:
        lst[i-1] = 'X'
    turn_num = lst.count('X') + lst.count('O') + 1
    return "true,{},O,{},{},{},{},{},{},{},{},{},{}\n".format(turn_num,lst[0], lst[1], lst[2], lst[3], lst[4], lst[5], lst[6], lst[7], lst[8], pos)
    
#%%
    
def writeout_first(arg_lists):
    with open('reference_generated.csv', 'a') as file:
        for i in arg_lists:
            file.write(gen_board_cond_x_first(i[0], i[1], i[2]))
            file.write(gen_board_cond_o_first(i[0], i[1], i[2]))
            
            
#%%
conditions_for_going_first = [
        [[],[],1],     # If going first, pick 1
        [[1],[5],9]]   # Take opposite corner if they took the middle
        
#%%
writeout_first(conditions_for_going_first)
writeout_second(conditions_for_going_second)

#%%

#%%s