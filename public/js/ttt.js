function Interface() {
	
	// Set up the board as part of the game class 
	this.board =  new Board();
	
	// ---------------------------------------------------------- //
	
	
	//
	this.main_div = document.getElementById("main_interface_div");
	
	//
	this.active_player_div = document.getElementById("active_player_display_div");
	
	//
	this.player_1_marker_div = document.getElementById("player_1_marker_div");
	this.player_1_type_div = document.getElementById("player_1_type_div");
	
	//
	this.player_2_marker_div = document.getElementById("player_2_marker_div");
	this.player_2_type_div = document.getElementById("player_2_type_div");
	
	// ---------------------------------------------------------- //
	
	this.remove_setup_form = function() {
		this.main_div.removeChild(this.main_div.childNodes[1]);
	};
	// ---------------------------------------------------------- //
	this.set_active_player_div = function(active_player) {
		this.active_player_div.innerText = active_player;
	};
	// ---------------------------------------------------------- //
	this.set_player_1_marker_div = function(marker) {
		this.player_1_marker_div.innerText = marker;
	};
	this.set_player_1_type_div = function(type) {
		this.player_1_type_div.innerText = type;
	};
	
	this.set_player_2_marker_div = function(marker) {
		this.player_2_marker_div.innerText = marker;
	};
	this.set_player_2_type_div = function(type) {
		this.player_2_type_div.innerText = type;
	};
	
	
}


interface = new Interface()

interface.remove_setup_form()

//interface.set_active_player_div("Hey");

interface.set_player_1_marker_div("Hey");
interface.set_player_1_type_div("Hey");

interface.set_player_2_marker_div("Hey");
interface.set_player_2_type_div("Hey");
