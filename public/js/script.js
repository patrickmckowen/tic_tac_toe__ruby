var tiles=document.getElementsByClassName("tile"); // Select all squares into an array
var state=[0,0,0,0,0,0,0,0,0];                     // Tracks what is in each space
var game=true;                                     // Tracks...
var visible=false;                                 // 
var PLAYER1=false;                                   
var PLAYER2=true;
var P1VAL=-1;
var P2VAL=1;
var currentPlayer=PLAYER1;
var winMatrix=[[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]];

// -------------------------------------------------------------------- //

var p1_type = document.getElementById("player_1_type_div").innerText.slice(6)
console.log("p1_type = " + p1_type)
var p1_name = document.getElementById("player_1_name_div").innerText
console.log("p1_name = " + p1_name)


var p2_type = document.getElementById("player_2_type_div").innerText.slice(6)
console.log("p2type = " + p2_type)

var p2_name = document.getElementById("player_2_name_div").innerText
console.log("p2type = " + p2_name)

// -------------------------------------------------------------------- //
// Call aiturn() if the active player is an unbeatable
function call_ai() {
	// If the current player is an ai
	if (currentPlayer==PLAYER1 && p1_type == "ROBOT_UNBEATABLE" || currentPlayer==PLAYER2 && p2_type == "ROBOT_UNBEATABLE") {
			aiturn(state, 0, currentPlayer, true);
	}
}
// -------------------------------------------------------------------- //
window.onload = call_ai();
// -------------------------------------------------------------------- //

function submit_results(winner) {
	document.getElementById('winner_input').value = winner;
	document.getElementById('results_form').submit();
}

// -------------------------------------------------------------------- //
function check_for_winner() {
	for(var x; x<9; x++) {
		if (x.hasAttribute("style")) {
				
		}
		
	}
	
}
// -------------------------------------------------------------------- //

// -------------------------------------------------------------------- //

// -------------------------------------------------------------------- //
// #################################################################### //

function reset() {
    for(var x=0;x<9;x++) {
		tiles[x].style.background="#fff";
		tiles[x].innerHTML="";
		state[x]=0;
    }
    game=true;
}

function toggle() {
    for(var x=0;x<9;x++)
	tiles[x].style.fontSize=visible?"0px":"1.5vh";
	document.getElementById("toggle").innerHTML=visible?"Show Values":"Hide Values";
    visible=!visible;
}

function claim(clicked) {
    if(!game)
		return;
    for(var x=0;x<9;x++) {
		if(tiles[x]==clicked&&state[x]==0) {
			set(x,currentPlayer);
		}
    }
    call_ai()
}

function set(index,player){
    if(!game)
	return;
    if(state[index]==0) {
		tiles[index].style.background=player==PLAYER1?"#22f":"#f22";
		state[index]=player==PLAYER1?P1VAL:P2VAL;
		currentPlayer=!currentPlayer;
		aiturn(state,0,currentPlayer,false);
			if(checkWin(state,player)||checkFull(state)) {
				for(var x=0;x<9;x++)
					tiles[x].innerHTML="";
					game=false;
			}
	}
}

function checkWin(board,player) {
	// Set value and player
	var value=player==PLAYER1?P1VAL:P2VAL;
	for(var x=0;x<8;x++) {
		var win=true;
		for(var y=0;y<3;y++) {
			if(board[winMatrix[x][y]]!=value) {
				win=false;
				break;
			}
		}
		if(win) {
			return true;
		}	
	}
	return false;
}

function checkFull(board) {
	for(var x=0;x<9;x++) {
		if(board[x]==0) {
			return false;
		}
	}
	return true;
}

function aiturn(board,depth,player,turn) {
	if(checkWin(board,!player))
		return-10+ depth;
	if(checkFull(board))
		return 0;
	var value=player==PLAYER1?P1VAL:P2VAL;
	var max=-Infinity;
	var index=0;
	for(var x=0;x<9;x++) {
		if(depth==0)
			tiles[x].innerHTML="";
			if(board[x]==0) {
				var newboard=board.slice();
				newboard[x]=value;
				var moveval=-aiturn(newboard,depth+ 1,!player,false);
				if(depth==0)
					tiles[x].innerHTML=moveval;
				if(moveval>max) {
					max=moveval;
					index=x;
				}
			}
	}
	if(turn)
		set(index,player)
		return max;
}
