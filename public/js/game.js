
function Game(board_obj) {
	
	this.interface = {
		'main_div': document.getElementById("main_interface_div"),
		'active_player_div': document.getElementById("active_player_display_div"),
		'layer_1_marker_div': document.getElementById("player_1_marker_div"),
		'player_1_type_div': document.getElementById("player_1_type_div"),
		'player_2_marker_div': document.getElementById("player_2_marker_div"),
		'board': [
		{'choice': '1', 'element' : document.getElementById("top_left")},
		{'choice': '2', 'element' : document.getElementById("top_center")},
		{'choice': '3', 'element' : document.getElementById("top_right")},

		{'choice': '4', 'element' : document.getElementById("middle_left")},
		{'choice': '5', 'element' : document.getElementById("middle_center")},
		{'choice': '6', 'element' : document.getElementById("middle_right")},

		{'choice': '7', 'element' : document.getElementById("bottom_left")},
		{'choice': '8', 'element' : document.getElementById("bottom_middle")},
		{'choice': '9', 'element' : document.getElementById("bottom_right")}
		],
		
		'place_mark' = function(marker, choice) {
		console.log('Marker: ' + marker);
		console.log('Choice: ' + choice);
		
		if (this.spot_is_open(choice)) {
			// Place the marker
			this.board[choice].innerHTML = marker
			// Delete this option now
			delete this.board[choice]
		}	
	};
	}
	
	//
	this.players = {
		'1' : undefined,
		'2' : undefined,
	}
	
	// Declare active player var
	this.active_player;
	
	// ------------------------------------------------- //
	
	//
	this.setup_players = function(player_1_type, player_2_type) {
		
		if (player_1_type == 'human') {
			this.players['1'] = new Human();
		} else if (player_1_type == 'sequential') {
			this.players['1'] = new Sequential();
		} else if (player_1_type == 'random') {
			this.players['1'] = new Random();
		} else if (player_1_type == 'unbeatable') {
			this.players['1'] = new Unbeatable();
		}
		
		if (player_2_type == 'human') {
			this.players['2'] = new Human();
		} else if (player_2_type == 'sequential') {
			this.players['2'] = new Sequential();
		} else if (player_2_type == 'random') {
			this.players['2'] = new Random();
		} else if (player_2_type == 'unbeatable') {
			this.players['2'] = new Unbeatable();
		}
	};
	
	// ------------------------------------------------- //
	// Switch the active player variable
	this.switch_active_player = function() {
		// 
		if (this.active_player === this.players['1']) {
			this.active_player = this.players['2'];
			console.log('Active player was 1, now is two')
		} else if (active_player == 'player 2') {
			this.active_player = this.players['1'];
			console.log('Active player was 2, now is 1')
		} else {
			this.active_player = this.players['1'];
			console.log('Active player was not set, now is 1')
		}
		// Update the div that shows the current player and the dive that shows current marker
		active_player_div.innerText = this.active_player.marker;
	};
	// ------------------------------------------------- //
}

game = new Game()

game.switch_active_player()

game.switch_active_player()
