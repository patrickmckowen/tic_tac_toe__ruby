
class Player_test < Minitest::Test

	def test__player__get_valid_coords
		player = Player.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, player.get_valid_coords)
	end
	
	def test__get_valid_choices
		player = Player.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, player.get_valid_choices)
	end
	
	def test__name
		player = Player.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, player.name)
	end
	
	def test__marker
		player = Player.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, player.marker)
	end
	
	def test__get_array
		player = Player.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, player.get_array)
	end
	
	def test__get_opponent_marker
		player = Player.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, player.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		player = Player.new('Steve', 'X')
		goal = true
		assert_equal(goal, player.went_first?(board.array))
		board.reset()
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		player = Player.new('Steve', 'X')
		goal = false
		assert_equal(goal, player.went_first?(board.array))
		board.reset()
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		player = Player.new('Steve', 'X')
		goal = 3
		assert_equal(goal, player.turn_num(board.array))
		board.reset()
	end
	
end
