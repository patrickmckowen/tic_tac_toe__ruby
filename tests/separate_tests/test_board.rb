
class Main_test < Minitest::Test


####################################################################

	# Accessing tt vars in board class 
	def test_6_board__valid_coords
		board = Board.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, board.get_valid_coords)
	end

####################################################################

	def test_7_board__array
		board = Board.new()
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
				
		assert_equal(goal, board.get_array())
	end

####################################################################

	# Accessing inherited var
	def test_8
		board = Board.new()
		goal = [[[1, 1], [1, 5], [1, 9]],     # Horizantal
			   [[5, 1], [5, 5], [5, 9]],     # Horizantal
			   [[9, 1], [9, 5], [9, 9]],  # Horizantal
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 5], [5, 5], [9, 5]],     # Vertical
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 1], [5, 5], [9, 9]],    # Diagonal
			   [[9, 1], [5, 5], [1, 9]]]    # Diagonal
		assert_equal(goal, board.get_winning_combinations)
	end
	
####################################################################

	def test_8_board__open_coords
		board = Board.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, board.get_open_coords)
	end

####################################################################	


	def test_9_board__remove_open_position
		board = Board.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5]]
		board.remove_open_coords([9, 9])
		assert_equal(goal, board.get_open_coords)
		board.reset()
	end
	
####################################################################

	def test_7_board__marker_at_blank
		board = Board.new()
		goal = ' '
		assert_equal(goal, board.marker_at([9, 9]))
	end

####################################################################


	def test_7_board__mark__x_at_9_9
		board = Board.new()
		board.mark('X', [9, 9])
		goal = 'X'
		assert_equal(goal, board.marker_at([9, 9]))
		board.reset()
	end



####################################################################

	def test_10_board__mark
		board = Board.new()
		board.mark('X', [1, 1])
		goal1 = 'X'
		goal2 = [[1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal1, board.marker_at([1, 1]))
		assert_equal(goal2, board.get_open_coords)
		board.reset()
	end

####################################################################
	
	# Check if board is full
	def test__board__full__false
		board = Board.new()
		board.mark('X', [9, 5])
		board.mark('X', [9, 9])
		goal = false
		assert_equal(goal, board.full?())
		board.reset()
	end

####################################################################

	# Check if board is full
	def test__board__full__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('X', [5, 1])
		board.mark('X', [5, 5])
		board.mark('X', [5, 9])
		board.mark('X', [9, 1])
		board.mark('X', [9, 5])
		board.mark('X', [9, 9])
		goal = true
		assert_equal(goal, board.full?())
		board.reset()
	end

####################################################################
	
	# Collect the characters from variable number of coordinates
	def test__board__collect__characters
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('X', [5, 1])
		board.mark('X', [5, 10])
		goal = ['X', 'X', 'X', ' ', 'X']
		assert_equal(goal, board.collect_characters([1, 1], [1, 5], [1, 9], [5, 5], [5, 10]))
		board.reset()
	end

####################################################################

	# Check if and array of characters is of the same character
	def test__board__same_characters__false
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('O', [5, 1])
		board.mark('X', [5, 10])
		returned_array = board.same_characters?([[1, 1], [1, 5], [1, 9], [5, 10]])
		goal = false
		assert_equal(goal, returned_array)
		board.reset()
	end

####################################################################

	def test__board__same_characters__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('X', [5, 1])
		board.mark('X', [5, 10])
		goal = true
		returned_array = board.collect_characters([1, 1], [1, 5], [1, 9], [5, 1],  [5, 10])
		assert_equal(goal, board.same_characters?(returned_array))
		board.reset()
	end

####################################################################
	
	def test__board__winner__top_row__nil
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [1, 5])
		assert_nil(board.winner())
		board.reset()
	end

####################################################################

	def test__board__winner__top_row__x
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [1, 5])
		board.mark("X", [1, 9])
		goal = "X"
		assert_equal(goal, board.winner())
		board.reset()
	end

####################################################################

	def test__board__winner__top_row__o
		board = Board.new()
		board.mark("O", [1, 1])
		board.mark("O", [1, 5])
		board.mark("O", [1, 9])
		goal = "O"
		assert_equal(goal, board.winner())
		board.reset()
	end

####################################################################

	def test_21_check_for_winning_combinations__diagonal__x
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [5, 5])
		board.mark("X", [9, 9])
		goal = "X"
		assert_equal(goal, board.winner())
		board.reset()
	end

####################################################################

	def test_22__board__winner__diagonal__nil
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [9, 5])
		board.mark("X", [9, 9])
		goal = nil
		assert_nil(board.winner())
		board.reset()
	end

####################################################################

	def test_23_board__winner__empty__none
		board = Board.new()
		# Not placing any tiles (empty board)
		goal = nil
		assert_nil(board.winner())
	end
	
####################################################################

	def test_24_board__position_empty
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [9, 5])
		board.mark("X", [9, 9])
		goal = false
		assert_equal(goal, board.position_empty?([1, 1]))
		board.reset()
	end

####################################################################

	def test_25_board__position_empty
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [9, 5])
		board.mark("X", [9, 9])
		goal = true
		assert_equal(goal, board.position_empty?([1, 5]))
		board.reset()
	end

####################################################################

	def test__board__choice_to_coordinates
		board = Board.new()
		goal = [1, 1]
		assert_equal(goal, board.choice_to_coordinates('1'))
	end

####################################################################

	def test_27__board__coords_to_choice
		board = Board.new()
		goal = '1'
		assert_equal(goal, board.coordinates_to_choice([1, 1]))
	end
	
####################################################################

	def test_28__board__handle_choice__1
		board = Board.new()
		board.handle_choice('X', '1')
		goal = 'X'
		assert_equal(goal, board.marker_at([1, 1]))
		board.reset()
	end
	
####################################################################
	
	def test__board__open_choices__all
		board = Board.new()
		goal = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
		assert_equal(goal, board.get_open_choices())
	end

####################################################################

	def test_29__board__open_choices__1_to_6
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		board.handle_choice('X', '9')
		goal = ["1", "2", "3", "4", "5", "6"]
		assert_equal(goal, board.get_open_choices())
		board.reset()
	end

####################################################################
end
