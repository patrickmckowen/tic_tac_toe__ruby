
class Sequential_test < Minitest::Test

	def test__get_valid_coords
		robot = Robot_sequential.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, robot.get_valid_coords)
	end
	
	def test__get_valid_choices
		robot = Robot_sequential.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, robot.get_valid_choices)
	end
	
	def test__name
		robot = Robot_sequential.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, robot.name)
	end
	
	def test__marker
		robot = Robot_sequential.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, robot.marker)
	end
	
	def test__get_array
		robot = Robot_sequential.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, robot.get_array)
	end
	
	def test__get_opponent_marker
		robot = Robot_sequential.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, robot.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		robot = Robot_sequential.new('Steve', 'X')
		goal = true
		assert_equal(goal, robot.went_first?(board.get_array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		robot = Robot_sequential.new('Steve', 'X')
		goal = false
		assert_equal(goal, robot.went_first?(board.get_array))
		board.mark(' ', [1, 1])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		robot = Robot_sequential.new('Steve', 'X')
		goal = 3
		assert_equal(goal, robot.turn_num(board.array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__choice__1
		robot = Robot_sequential.new('Steve', "X")
		board = Board.new()
		goal = '1'
		assert_equal(goal, robot.choice(board.array, board.get_open_choices))
		
		board.mark(' ', [1, 1])

		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end

####################################################################

	def test__choice__9
		robot = Robot_sequential.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		goal = '9'
		assert_equal(goal, robot.choice(board.array, board.get_open_choices))
		
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.mark(' ', [1, 9])
		board.mark(' ', [5, 1])
		board.mark(' ', [5, 5])
		board.mark(' ', [5, 9])
		board.mark(' ', [9, 1])
		board.mark(' ', [9, 5])

		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.add_open_coords([1, 9]) # put it back
		board.add_open_choice('3') # put it back
		board.add_open_coords([5, 1]) # put it back
		board.add_open_choice('4') # put it back
		board.add_open_coords([5, 5]) # put it back
		board.add_open_choice('5') # put it back
		board.add_open_coords([5, 9]) # put it back
		board.add_open_choice('6') # put it back
		board.add_open_coords([9, 1]) # put it back
		board.add_open_choice('7') # put it back
		board.add_open_coords([9, 5]) # put it back
		board.add_open_choice('8') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
####################################################################
	
	def test__choice__false
		robot = Robot_sequential.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '9')
		goal = '8'
		assert_equal(goal, robot.choice(board.array, board.get_open_choices))
		
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.mark(' ', [1, 9])
		board.mark(' ', [5, 1])
		board.mark(' ', [5, 5])
		board.mark(' ', [5, 9])
		board.mark(' ', [9, 1])
		board.mark(' ', [9, 9])

		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.add_open_coords([1, 9]) # put it back
		board.add_open_choice('3') # put it back
		board.add_open_coords([5, 1]) # put it back
		board.add_open_choice('4') # put it back
		board.add_open_coords([5, 5]) # put it back
		board.add_open_choice('5') # put it back
		board.add_open_coords([5, 9]) # put it back
		board.add_open_choice('6') # put it back
		board.add_open_coords([9, 1]) # put it back
		board.add_open_choice('7') # put it back
		board.add_open_coords([9, 9]) # put it back
		board.add_open_choice('9') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
		
	end
	
end
