########################################################################################################################################

# test__ttt__valid_positions
# test__ttt__valid_responses

####################################################################

require "minitest/autorun"

####################################################################

require_relative '../ttt/ttt.rb'
require_relative '../ttt/board/board.rb'
require_relative '../ttt/game/game.rb'
require_relative '../ttt/player/player.rb'
require_relative '../ttt/player/human/human.rb'
require_relative '../ttt/player/robot/sequential/sequential.rb'
require_relative '../ttt/player/robot/random/random.rb'
require_relative '../ttt/player/robot/unbeatable/unbeatable.rb'

########################################################################################################################################

class Main_test < Minitest::Test


####################################################################

	# Accessing tt vars in board class 
	def test_6_board__valid_coords
		board = Board.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, board.get_valid_coords)
	end

####################################################################

	def test_7_board__array
		board = Board.new()
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "], 
				[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
				
		assert_equal(goal, board.get_array())
	end

####################################################################

	# Accessing inherited var
	def test_8
		board = Board.new()
		goal = [[[1, 1], [1, 5], [1, 9]],     # Horizantal
			   [[5, 1], [5, 5], [5, 9]],     # Horizantal
			   [[9, 1], [9, 5], [9, 9]],  # Horizantal
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 5], [5, 5], [9, 5]],     # Vertical
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 1], [5, 5], [9, 9]],    # Diagonal
			   [[9, 1], [5, 5], [1, 9]]]    # Diagonal
		assert_equal(goal, board.get_winning_combinations)
	end
	
####################################################################

	def test_8_board__open_coords
		board = Board.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, board.get_open_coords)
	end

####################################################################	


	def test_9_board__remove_open_position
		board = Board.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5]]
		board.remove_open_coords([9, 9])
		assert_equal(goal, board.get_open_coords)
		board.reset()
	end
	
####################################################################

	def test_7_board__marker_at_blank
		board = Board.new()
		goal = ' '
		assert_equal(goal, board.marker_at([9, 9]))
	end

####################################################################


	def test_7_board__mark__x_at_9_9
		board = Board.new()
		board.mark('X', [9, 9])
		goal = 'X'
		assert_equal(goal, board.marker_at([9, 9]))
		board.reset()
	end



####################################################################

	def test_10_board__mark
		board = Board.new()
		board.mark('X', [1, 1])
		goal1 = 'X'
		goal2 = [[1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal1, board.marker_at([1, 1]))
		assert_equal(goal2, board.get_open_coords)
		board.reset()
	end

####################################################################
	
	# Check if board is full
	def test__board__full__false
		board = Board.new()
		board.mark('X', [9, 5])
		board.mark('X', [9, 9])
		goal = false
		assert_equal(goal, board.full?())
		board.reset()
	end

####################################################################

	# Check if board is full
	def test__board__full__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('X', [5, 1])
		board.mark('X', [5, 5])
		board.mark('X', [5, 9])
		board.mark('X', [9, 1])
		board.mark('X', [9, 5])
		board.mark('X', [9, 9])
		goal = true
		assert_equal(goal, board.full?())
		board.reset()
	end

####################################################################
	
	# Collect the characters from variable number of coordinates
	def test__board__collect__characters
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('X', [5, 1])
		board.mark('X', [5, 10])
		goal = ['X', 'X', 'X', ' ', 'X']
		assert_equal(goal, board.collect_characters([1, 1], [1, 5], [1, 9], [5, 5], [5, 10]))
		board.reset()
	end

####################################################################

	# Check if and array of characters is of the same character
	def test__board__same_characters__false
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('O', [5, 1])
		board.mark('X', [5, 10])
		returned_array = board.same_characters?([[1, 1], [1, 5], [1, 9], [5, 10]])
		goal = false
		assert_equal(goal, returned_array)
		board.reset()
	end

####################################################################

	def test__board__same_characters__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('X', [1, 5])
		board.mark('X', [1, 9])
		board.mark('X', [5, 1])
		board.mark('X', [5, 10])
		goal = true
		returned_array = board.collect_characters([1, 1], [1, 5], [1, 9], [5, 1],  [5, 10])
		assert_equal(goal, board.same_characters?(returned_array))
		board.reset()
	end

####################################################################
	
	def test__board__winner__top_row__nil
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [1, 5])
		assert_nil(board.winner())
		board.reset()
	end

####################################################################

	def test__board__winner__top_row__x
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [1, 5])
		board.mark("X", [1, 9])
		goal = "X"
		assert_equal(goal, board.winner())
		board.reset()
	end

####################################################################

	def test__board__winner__top_row__o
		board = Board.new()
		board.mark("O", [1, 1])
		board.mark("O", [1, 5])
		board.mark("O", [1, 9])
		goal = "O"
		assert_equal(goal, board.winner())
		board.reset()
	end

####################################################################

	def test_21_check_for_winning_combinations__diagonal__x
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [5, 5])
		board.mark("X", [9, 9])
		goal = "X"
		assert_equal(goal, board.winner())
		board.reset()
	end

####################################################################

	def test_22__board__winner__diagonal__nil
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [9, 5])
		board.mark("X", [9, 9])
		goal = nil
		assert_nil(board.winner())
		board.reset()
	end

####################################################################

	def test_23_board__winner__empty__none
		board = Board.new()
		# Not placing any tiles (empty board)
		goal = nil
		assert_nil(board.winner())
	end
	
####################################################################

	def test_24_board__position_empty
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [9, 5])
		board.mark("X", [9, 9])
		goal = false
		assert_equal(goal, board.position_empty?([1, 1]))
		board.reset()
	end

####################################################################

	def test_25_board__position_empty
		board = Board.new()
		board.mark("X", [1, 1])
		board.mark("X", [9, 5])
		board.mark("X", [9, 9])
		goal = true
		assert_equal(goal, board.position_empty?([1, 5]))
		board.reset()
	end

####################################################################

	def test__board__choice_to_coordinates
		board = Board.new()
		goal = [1, 1]
		assert_equal(goal, board.choice_to_coordinates('1'))
	end

####################################################################

	def test_27__board__coords_to_choice
		board = Board.new()
		goal = '1'
		assert_equal(goal, board.coordinates_to_choice([1, 1]))
	end
	
####################################################################

	def test_28__board__handle_choice__1
		board = Board.new()
		board.handle_choice('X', '1')
		goal = 'X'
		assert_equal(goal, board.marker_at([1, 1]))
		board.reset()
	end
	
####################################################################
	
	def test__board__open_choices__all
		board = Board.new()
		goal = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
		assert_equal(goal, board.get_open_choices())
	end

####################################################################

	def test_29__board__open_choices__1_to_6
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		board.handle_choice('X', '9')
		goal = ["1", "2", "3", "4", "5", "6"]
		assert_equal(goal, board.get_open_choices())
		board.reset()
	end

####################################################################
end

class Game_test < Minitest::Test


	def test__setup_players
		print("Type 's' for player 1 and something else for p 2. \n\n")
		game = Game.new()
		#game.player_1 = Robot_sequential.new('Steve', 'O')
		#game.player_2 = Robot_sequential.new('Mike', 'X')
		game.setup_players()
		goal = 's'
		assert_equal(goal, game.active_player.name)
	end

	def test__active_player__without_switch
		#print("Type 's' for player 2 and something else for p 1. \n\n")
		game = Game.new()
		game.player_1 = Robot_sequential.new('Steve', 'O')
		game.player_2 = Robot_sequential.new('Mike', 'X')
		game.active_player = game.player_1
		game.inactive_player = game.player_2
		goal = 'Steve'
		assert_equal(goal, game.active_player.name)
	end


	def test__active_player__with_switch
		#print("Type 'm' \n\n")
		game = Game.new()
		game.player_1 = Robot_sequential.new('Steve', 'O')
		game.player_2 = Robot_sequential.new('Mike', 'X')
		game.active_player = game.player_1
		game.inactive_player = game.player_2
		game.switch_active_player
		goal = 'Mike'
		assert_equal(goal, game.active_player.name)
	end
	

end

########################################################################################################################################

class Human_test < Minitest::Test
	
	def test__opponent_marker
		human = Human.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, human.opponent_marker)
	end

####################################################################
	def test__get_valid_coords
		human = Human.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, human.get_valid_coords)
	end
	
	def test__get_valid_choices
		human = Human.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, human.get_valid_choices)
	end
	
	def test__name
		human = Human.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, human.name)
	end
	
	def test__marker
		human = Human.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, human.marker)
	end
	
	def test__get_array
		human = Human.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, human.get_array)
	end
	
	def test__get_opponent_marker
		human = Human.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, human.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		human = Human.new('Steve', 'X')
		goal = true
		assert_equal(goal, human.went_first?(board.array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		human = Human.new('Steve', 'X')
		goal = false
		assert_equal(goal, human.went_first?(board.array))
		board.mark(' ', [1, 1])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		human = Human.new('Steve', 'X')
		goal = 3
		assert_equal(goal, human.turn_num(board.array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end


####################################################################

	def test__human__choice__pick_1
		human = Human.new('Steve', 'X')
		board = Board.new()
		goal = '1'
		print("Choose 1 \n \n")
		assert_equal(goal, human.choice(board.array, board.get_open_choices))
	end

####################################################################

	def test__human__choice__pick_2
		human = Human.new('Steve', 'X')
		board = Board.new()
		goal = '2'
		print("Choose 2 \n\n")
		assert_equal(goal, human.choice(board.array, board.get_open_choices))
	end

####################################################################

	def test__human__choice__pick_6
		human = Human.new('Steve', 'X')
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		board.handle_choice('X', '9')
		goal = '6'
		print("Choose 9, then 8, then, 7, then 6 \n\n")
		assert_equal(goal, human.choice(board.array, board.get_open_choices))
		board.mark(' ', [9, 1])
		board.mark(' ', [9, 5])
		board.mark(' ', [9, 9])
		board.add_open_coords([9, 1]) # put it back
		board.add_open_coords([9, 5]) # put it back
		board.add_open_coords([9, 9]) # put it back
		board.add_open_choice('7') # put it back
		board.add_open_choice('8') # put it back
		board.add_open_choice('9') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end

end

class Player_test < Minitest::Test

	def test__player__get_valid_coords
		player = Player.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, player.get_valid_coords)
	end
	
	def test__get_valid_choices
		player = Player.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, player.get_valid_choices)
	end
	
	def test__name
		player = Player.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, player.name)
	end
	
	def test__marker
		player = Player.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, player.marker)
	end
	
	def test__get_array
		player = Player.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, player.get_array)
	end
	
	def test__get_opponent_marker
		player = Player.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, player.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		player = Player.new('Steve', 'X')
		goal = true
		assert_equal(goal, player.went_first?(board.array))
		board.reset()
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		player = Player.new('Steve', 'X')
		goal = false
		assert_equal(goal, player.went_first?(board.array))
		board.reset()
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		player = Player.new('Steve', 'X')
		goal = 3
		assert_equal(goal, player.turn_num(board.array))
		board.reset()
	end
	
end

class Random_test < Minitest::Test

	def test__random__get_valid_coords
		random = Robot_random.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, random.get_valid_coords)
	end
	
	def test__get_valid_choices
		random = Robot_random.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, random.get_valid_choices)
	end
	
	def test__name
		random = Robot_random.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, random.name)
	end
	
	def test__marker
		random = Robot_random.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, random.marker)
	end
	
	def test__get_array
		random = Robot_random.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, random.get_array)
	end
	
	def test__get_opponent_marker
		random = Robot_random.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, random.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		random = Robot_random.new('Steve', 'X')
		goal = true
		assert_equal(goal, random.went_first?(board.get_array))
		board.reset()
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		random = Robot_random.new('Steve', 'X')
		goal = false
		board.reset()
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		random = Robot_random.new('Steve', 'X')
		goal = 3
		assert_equal(goal, random.turn_num(board.array))
		board.reset()
	end
	
	def test__choice__1
		robot = Robot_random.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		board.handle_choice('X', '9')
		goal = '1'
		assert_equal(goal, robot.choice(board.array, board.open_choices))
		board.reset()
	end

####################################################################

	def test__choice__9
		robot = Robot_random.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		goal = '9'
		assert_equal(goal, robot.choice(board.array, board.open_choices))
		board.reset()
	end
	
####################################################################
	
	def test__choice__false
		robot = Robot_random.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '9')
		goal = '8'
		assert_equal(goal, robot.choice(board.array, board.open_choices))
		board.reset()
		
	end
	
end

class Sequential_test < Minitest::Test

	def test__get_valid_coords
		robot = Robot_sequential.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, robot.get_valid_coords)
	end
	
	def test__get_valid_choices
		robot = Robot_sequential.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, robot.get_valid_choices)
	end
	
	def test__name
		robot = Robot_sequential.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, robot.name)
	end
	
	def test__marker
		robot = Robot_sequential.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, robot.marker)
	end
	
	def test__get_array
		robot = Robot_sequential.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, robot.get_array)
	end
	
	def test__get_opponent_marker
		robot = Robot_sequential.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, robot.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		robot = Robot_sequential.new('Steve', 'X')
		goal = true
		assert_equal(goal, robot.went_first?(board.get_array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		robot = Robot_sequential.new('Steve', 'X')
		goal = false
		assert_equal(goal, robot.went_first?(board.get_array))
		board.mark(' ', [1, 1])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		robot = Robot_sequential.new('Steve', 'X')
		goal = 3
		assert_equal(goal, robot.turn_num(board.array))
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
	def test__choice__1
		robot = Robot_sequential.new('Steve', "X")
		board = Board.new()
		goal = '1'
		assert_equal(goal, robot.choice(board.array, board.get_open_choices))
		
		board.mark(' ', [1, 1])

		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end

####################################################################

	def test__choice__9
		robot = Robot_sequential.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		goal = '9'
		assert_equal(goal, robot.choice(board.array, board.get_open_choices))
		
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.mark(' ', [1, 9])
		board.mark(' ', [5, 1])
		board.mark(' ', [5, 5])
		board.mark(' ', [5, 9])
		board.mark(' ', [9, 1])
		board.mark(' ', [9, 5])

		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.add_open_coords([1, 9]) # put it back
		board.add_open_choice('3') # put it back
		board.add_open_coords([5, 1]) # put it back
		board.add_open_choice('4') # put it back
		board.add_open_coords([5, 5]) # put it back
		board.add_open_choice('5') # put it back
		board.add_open_coords([5, 9]) # put it back
		board.add_open_choice('6') # put it back
		board.add_open_coords([9, 1]) # put it back
		board.add_open_choice('7') # put it back
		board.add_open_coords([9, 5]) # put it back
		board.add_open_choice('8') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
	end
	
####################################################################
	
	def test__choice__false
		robot = Robot_sequential.new('Steve', "X")
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '2')
		board.handle_choice('X', '3')
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		board.handle_choice('X', '6')
		board.handle_choice('X', '7')
		board.handle_choice('X', '9')
		goal = '8'
		assert_equal(goal, robot.choice(board.array, board.get_open_choices))
		
		board.mark(' ', [1, 1])
		board.mark(' ', [1, 5])
		board.mark(' ', [1, 9])
		board.mark(' ', [5, 1])
		board.mark(' ', [5, 5])
		board.mark(' ', [5, 9])
		board.mark(' ', [9, 1])
		board.mark(' ', [9, 9])

		board.add_open_coords([1, 1]) # put it back
		board.add_open_choice('1') # put it back
		board.add_open_coords([1, 5]) # put it back
		board.add_open_choice('2') # put it back
		board.add_open_coords([1, 9]) # put it back
		board.add_open_choice('3') # put it back
		board.add_open_coords([5, 1]) # put it back
		board.add_open_choice('4') # put it back
		board.add_open_coords([5, 5]) # put it back
		board.add_open_choice('5') # put it back
		board.add_open_coords([5, 9]) # put it back
		board.add_open_choice('6') # put it back
		board.add_open_coords([9, 1]) # put it back
		board.add_open_choice('7') # put it back
		board.add_open_coords([9, 9]) # put it back
		board.add_open_choice('9') # put it back
		board.get_open_choices.sort!
		board.get_open_coords.sort!
		
	end
	
end

class Ttt_test < Minitest::Test

	# Instance variable of master class - valid_positions
	def test__ttt__valid_coords
		ttt= Ttt.new()
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, ttt.get_valid_coords)
	end

####################################################################

	# Instance variable of master class - valid_responses
	def test__ttt__valid_choices
		ttt= Ttt.new()
		goal = ['1','2','3','4','5','6','7','8','9']
		assert_equal(goal, ttt.get_valid_choices)
	end

####################################################################


	# Instance variable of master class - valid_positions
	def test__ttt__remove_open_coords
		ttt= Ttt.new()
		ttt.remove_open_coords([1, 1])
		goal = [[1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, ttt.get_open_coords)
	end

####################################################################

	# Instance variable of master class - valid_responses
	def test__ttt__open_choices
		ttt= Ttt.new()
		ttt.remove_open_choice('1')
		goal = ['2','3','4','5','6','7','8','9']
		assert_equal(goal, ttt.get_open_choices)
	end

####################################################################

	# Instance variable of master class - winning combinations
	def test_3_ttt__winning_combinations
		ttt= Ttt.new()
		goal = [[[1, 1], [1, 5], [1, 9]],     # Horizantal
			   [[5, 1], [5, 5], [5, 9]],     # Horizantal
			   [[9, 1], [9, 5], [9, 9]],  # Horizantal
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 5], [5, 5], [9, 5]],     # Vertical
			   [[1, 1], [5, 1], [9, 1]],     # Vertical
			   [[1, 1], [5, 5], [9, 9]],    # Diagonal
			   [[9, 1], [5, 5], [1, 9]]]    # Diagonal
			   
		assert_equal(goal, ttt.get_winning_combinations)
	end
	
####################################################################

	# Instance variable of master class - winning combinations
	def test_3_ttt__array
		ttt= Ttt.new()
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
			   
		assert_equal(goal, ttt.get_array)
	end

end #grtpies

class Game_test < Minitest::Test

	def test__unbeatable__get_valid_coords
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [[1, 1], [1, 5], [1, 9], [5, 1], [5, 5], [5, 9], [9, 1], [9, 5], [9, 9]]
		assert_equal(goal, unbeatable.get_valid_coords)
	end
	
	def test__get_valid_choices
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
		assert_equal(goal, unbeatable.get_valid_choices)
	end
	
	def test__name
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 'Steve'
		assert_equal(goal, unbeatable.name)
	end
	
	def test__marker
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 'X'
		assert_equal(goal, unbeatable.marker)
	end
	
	def test__get_array
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [[" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    ["#", "#", "#", "#", "#", "#", "#", "#", "#", "#", "#"],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "],
			    [" ", " ", " ", "#", " ", " ", " ", "#", " ", " ", " "]]
		assert_equal(goal, unbeatable.get_array)
	end
	
	def test__get_opponent_marker
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 'O'
		assert_equal(goal, unbeatable.opponent_marker)
	end
	
	def test__went_first__true
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = true
		assert_equal(goal, unbeatable.went_first?(board.get_array))
		board.reset()
	end
	
	def test__went_first__false
		board = Board.new()
		board.mark('O', [1, 1])
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = false
		assert_equal(goal, unbeatable.went_first?(board.array))
		board.reset()
	end
	
	def test__turn_num
		board = Board.new()
		board.mark('X', [1, 1])
		board.mark('O', [1, 5])
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = 3
		assert_equal(goal, unbeatable.turn_num(board.array))
		board.reset()
	end
	
####################################################################						
						
	def test__unbeatable__examine_winning_combinations__1
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('O', '2')
		board.handle_choice('O', '9')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [["O", "O", [1, 9]], [[5, 1], [5, 5], [5, 9]], [[9, 1], [9, 5], "O"], ["O", [5, 1], [9, 1]], ["O", [5, 5], [9, 5]], ["O", [5, 1], [9, 1]], ["O", [5, 5], "O"], [[9, 1], [5, 5], [1, 9]]]
		assert_equal(goal, unbeatable.examine_winning_combinations(board.get_array))
		board.reset()
	end
	
####################################################################

	def test__detect_two_in_a_row__o
		board = Board.new()
		board.handle_choice('O', '4')
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = ['O', [5, 9]]
		assert_equal(goal, unbeatable.detect_2_in_a_row(board.get_array))
		board.reset()
	end

####################################################################

	def test__detect_two_in_a_row__x
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = ['X', [5, 9]]
		assert_equal(goal, unbeatable.detect_2_in_a_row(board.get_array))
		board.reset()
	end


####################################################################

	# BLOCK MIDDLE ROW MIDDLE COLUMN
	def test_unbeatable__block__middle_row
		board = Board.new()
		board.handle_choice('O', '4')
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '6'
		assert_equal(goal, unbeatable.block__choice(board.get_array))
		board.reset()
	end

####################################################################

	# BLOCK BOTTOM ROW
	def test_unbeatable__block__bottom_row
		board = Board.new()
		board.handle_choice('O', '7')
		board.handle_choice('O', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.block__choice(board.get_array))
		board.reset()
	end

####################################################################

	# BLOCK DIAGONAL AT BOTTOM RIGHT
	def test_unbeatable__block__diagonal
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.block__choice(board.get_array))
		board.reset()
	end


####################################################################

	# 
	def test_unbeatable__win__middle_row
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('X', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '6'
		assert_equal(goal, unbeatable.win__choice(board.get_array))
		board.reset()
	end

####################################################################

	# WIN BOTTOM ROW
	def test_unbeatable__win__bottom_row
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.win__choice(board.get_array))
		board.reset()
	end

####################################################################

	# WIN DIAGONAL AT BOTTOM RIGHT
	def test_unbeatable__win__diagonal
		board = Board.new()
		board.handle_choice('X', '1')
		board.handle_choice('X', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.win__choice(board.get_array))
		board.reset()
	end

####################################################################

	# CSV TURN MATCH
	def test__unbeatable__csv__match_turn__1
		board = Board.new()
		board.handle_choice('X', '1')                                # Player went first = true
		board.handle_choice('O', '5')
		unbeatable = Robot_unbeatable.new('Steve', 'X')              # Player marker = X
		goal = [["O", "X", "X", "O", " ", "X", "X", " ", " ", "1"], [" ", " ", " ", "X", " ", " ", " ", " ", "O", "1"]]
		assert_equal(goal, unbeatable.csv__match_turn(board.get_array))
		board.reset()
	end

####################################################################

	# # CSV TURN MATCH
	def test__unbeatable__csv__match_turn__2
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('X', '5')                                #Player went first = false
		board.handle_choice('O', '6')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [["O", " ", " ", " ", "X", " ", " ", " ", "O", "2"], [" ", " ", "O", " ", "X", " ", " ", " ", " ", "5"], [" ", " ", " ", " ", "X", " ", "O", " ", " ", "5"], [" ", " ", " ", " ", "X", " ", " ", " ", "O", "5"], ["O", "X", "X", "O", " ", "X", "X", " ", " ", "1"], [" ", " ", "X", "O", " ", "X", "X", " ", " ", "1"]]
		assert_equal(goal, unbeatable.csv__match_turn(board.get_array))
		board.reset()
	end
	
####################################################################

	# # CSV board MATCH
	def test__board__csv__match_board__1
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('O', '9')                               
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = [" ", " ", " ", "X", " ", " ", " ", " ", "O", "1"]
		assert_equal(goal, unbeatable.csv__match_board(board.get_array))
		board.reset()
	end

####################################################################	

	# CSV choice
	# Selects the final digit from the return of csv__board_match
	def test__unbeatable__csv__choice__1
		board = Board.new()
		board.handle_choice('X', '4')
		board.handle_choice('O', '9')                             
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

########################################################################################################################################	
	
	# CSV choice
	# Selects the final digit from the return of csv__board_match
	def test__unbeatable__csv__choice__2nd_move__top_left
		board = Board.new()
		board.handle_choice('O', '1')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__unbeatable__csv__choice__2nd_move_top_right
		board = Board.new()
		board.handle_choice('X', '3')
		unbeatable = Robot_unbeatable.new('Steve', 'O')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__unbeatable__csv__choice__2nd_move__bottom_left
		board = Board.new()
		board.handle_choice('X', '7')
		unbeatable = Robot_unbeatable.new('Steve', 'O')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__unbeatable__csv__choice__2nd_move__bottom_right
		board = Board.new()
		board.handle_choice('X', '9')
		unbeatable = Robot_unbeatable.new('Steve', 'O')
		goal = '5'
		assert_equal(goal, unbeatable.csv__choice(board.get_array))
		board.reset()
	end

####################################################################

	def test__choice__block__1
		board = Board.new()
		board.handle_choice('O', '7')
		board.handle_choice('O', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__block__2
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('X', '8')
		board.handle_choice('O', '2')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '3'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__block__3
		board = Board.new()
		board.handle_choice('O', '1')
		board.handle_choice('X', '8')
		board.handle_choice('O', '2')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '3'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__win__1
		board = Board.new()
		board.handle_choice('X', '7')
		board.handle_choice('X', '8')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################

	def test__choice__win__2
		board = Board.new()
		board.handle_choice('X', '2')
		board.handle_choice('O', '8')
		board.handle_choice('X', '3')
		board.handle_choice('O', '6')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# Handling forks

	def test__choice__fork__1
		board = Board.new()
		board.handle_choice('X', '2')
		board.handle_choice('O', '8')
		board.handle_choice('X', '3')
		board.handle_choice('O', '6')
		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 
	def test__choice__csv___1
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '9')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___2
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '3')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___3
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '7')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___4
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '9')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___5
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '4')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___6
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '6')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___7
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '2')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '4'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___8
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '8')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '4'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___9
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '4')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___10
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '4')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___11
		board = Board.new()
		board.handle_choice('O', '1')

		board.handle_choice('X', '5')
		board.handle_choice('O', '4')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end


#############################################################################################################################################
#############################################################################################################################################
####################################################################
# 
	def test__choice__csv___13
		board = Board.new()
		board.handle_choice('O', '3')

		board.handle_choice('X', '5')
		board.handle_choice('O', '7')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___14
		board = Board.new()
		board.handle_choice('O', '3')

		board.handle_choice('X', '5')
		board.handle_choice('O', '1')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___15
		board = Board.new()
		board.handle_choice('O', '3')

		board.handle_choice('X', '5')
		board.handle_choice('O', '2')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___16
		board = Board.new()
		board.handle_choice('O', '3')

		board.handle_choice('X', '5')
		board.handle_choice('O', '4')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___17
		board = Board.new()
		board.handle_choice('O', '3')

		board.handle_choice('X', '5')
		board.handle_choice('O', '6')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___18
		board = Board.new()
		board.handle_choice('O', '3')

		board.handle_choice('X', '5')
		board.handle_choice('O', '9')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '6'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

###################################################################################################################################################
###################################################################################################################################################
####################################################################
# 
	def test__choice__csv___19
		board = Board.new()
		board.handle_choice('O', '7')

		board.handle_choice('X', '5')
		board.handle_choice('O', '1')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '4'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___20
		board = Board.new()
		board.handle_choice('O', '7')

		board.handle_choice('X', '5')
		board.handle_choice('O', '2')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '4'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___21
		board = Board.new()
		board.handle_choice('O', '7')

		board.handle_choice('X', '5')
		board.handle_choice('O', '3')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___22
		board = Board.new()
		board.handle_choice('O', '7')

		board.handle_choice('X', '5')
		board.handle_choice('O', '4')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '1'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___23
		board = Board.new()
		board.handle_choice('O', '7')

		board.handle_choice('X', '5')
		board.handle_choice('O', '6')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___24
		board = Board.new()
		board.handle_choice('O', '7')

		board.handle_choice('X', '5')
		board.handle_choice('O', '8')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '9'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___25
		board = Board.new()
		board.handle_choice('O', '7')

		board.handle_choice('X', '5')
		board.handle_choice('O', '9')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '8'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
################################################################################################################################
################################################################################################################################
####################################################################
# 
	def test__choice__csv___26
		board = Board.new()
		board.handle_choice('O', '9')

		board.handle_choice('X', '5')
		board.handle_choice('O', '1')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___27
		board = Board.new()
		board.handle_choice('O', '9')

		board.handle_choice('X', '5')
		board.handle_choice('O', '2')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '4'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end

####################################################################
# 

	def test__choice__csv___28
		board = Board.new()
		board.handle_choice('O', '9')

		board.handle_choice('X', '5')
		board.handle_choice('O', '3')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '6'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___29
		board = Board.new()
		board.handle_choice('O', '9')

		board.handle_choice('X', '5')
		board.handle_choice('O', '4')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '2'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___30
		board = Board.new()
		board.handle_choice('O', '9')

		board.handle_choice('X', '5')
		board.handle_choice('O', '6')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '3'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
# 

	def test__choice__csv___31
		board = Board.new()
		board.handle_choice('O', '9')

		board.handle_choice('X', '5')
		board.handle_choice('O', '7')

		unbeatable = Robot_unbeatable.new('Steve', 'X')
		goal = '8'
		assert_equal(goal, unbeatable.choice(board.get_array, board.get_open_coords))
		board.reset()
	end
####################################################################
end
####################################################################

