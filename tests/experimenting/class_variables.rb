
class Ttt
	attr_accessor :array, :open_spaces
	
	
	@@array = [[nil, nil, nil],
	           [nil, nil, nil],
	           [nil, nil, nil]]
	           
	@@open_spaces = [[0,0],[0,1],[0,2],
	                 [1,0],[1,1],[1,2],
	                 [2,0],[2,1],[2,2]]
	
	                 
	def remove_open_space(choice)
		@@open_spaces.delete(@@open_spaces[choice-1])
	end
	

	def open_spaces
		@@open_spaces
	end

end

class Player < Ttt
	def initialize(name, marker)
		@name = name
		@marker = marker
		#uper()
	end
end

steve = Player.new('Steve', 'X')

p(steve.open_spaces())
steve.remove_open_space(1)
p(steve.open_spaces())

