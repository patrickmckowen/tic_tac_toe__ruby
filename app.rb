
# --------------------------------------------------------------------------- #
                               # IMPORTS #
require 'sinatra'

require_relative 'ttt/ttt.rb'
require_relative 'ttt/board/board.rb'
require_relative 'ttt/game/game.rb'
require_relative 'ttt/player/player.rb'
require_relative 'ttt/player/human/human.rb'
require_relative 'ttt/player/robot/random/random.rb'
require_relative 'ttt/player/robot/sequential/sequential.rb'
require_relative 'ttt/player/robot/unbeatable/unbeatable.rb'
require_relative 'rds.rb'

enable :sessions

# ------------------------------------------------------------------------- #
                               # SETUP #

get '/' do
	erb :index
end

post '/setup' do
	session[:game] = Game.new()
	session[:game].reset()
	session[:game].setup_players_web_app(params[:player_1_name],
								 params[:player_1_type].to_i,
								 params[:player_2_name],
								 params[:player_2_type].to_i)
								 
	if session[:game].player_1.class == Robot_unbeatable || session[:game].player_2.class == Robot_unbeatable  
		redirect :tictactoe
	else
		redirect :tic_tac_toe
	end
end

# ------------------------------------------------------------------------- #
                               # GAME #
#
get '/tic_tac_toe' do
	
	# If active player is a robot
	if session[:game].active_player.class == Robot_sequential ||
	   session[:game].active_player.class == Robot_random ||
	   session[:game].active_player.class == Robot_unbeatable
		
		# Just get choice, don't render page
		session[:game].handle_player_choice_web_app()
		redirect :check_for_winner
	end
	
	erb :tic_tac_toe
end
# ----------------------------------------------------------- #
get '/tictactoe' do
	
	# If active player is a robot
	if session[:game].active_player.class == Robot_sequential ||
	   session[:game].active_player.class == Robot_random ||
	   session[:game].active_player.class == Robot_unbeatable
		
		# Just get choice, don't render page
		session[:game].handle_player_choice_web_app()
		redirect :check_for_winner
	end
	
	erb :tictactoe
end
# ---------------------------------------------------------- #
#
post '/tic_tac_toe' do
	# Handle form data if its a human
	session[:game].board.handle_choice(session[:game].active_player.marker,
									   params[:choice])
	redirect :check_for_winner
end
# --------------------------------------------------------------------------- #

get '/tictactoe' do
	
	
	erb :tictactoe
end

# --------------------------------------------------------------------------- #

get '/check_for_winner' do
	# Check for winner and set the winner var if neccessary
	session[:game].winner = session[:game].board.winner()
	
	# If the board is not full and there is no winner
	if session[:game].board.full?() == false && session[:game].winner == nil
		# SWITCH ACTIVE PLAYER
		session[:game].switch_active_player()
		# LOOP BACK
		redirect :tic_tac_toe
	else
		# GO TO RESULTS PAGE
		redirect :results
	end
	
end

# --------------------------------------------------------------------------- #

get '/results' do
	winner = ''
	if session[:game].board.winner() == nil
		winner = "Tied Game"
	else
		winner = session[:game].active_player.name
	end
		
	insert_game(session[:game].player_1.name,
		        session[:game].player_2.name,
		        winner)
	session[:winner_table] = generate_table()
	erb :results
end


get '/results_unbeatable' do
	insert_game(params[:player_1_name],
		        params[:player_2_name],
		        params[:winner_input])

	session[:winner_table] = generate_table()
	erb :results
end
