

# --------------------------------------------------------------------------- #
                           # IMPORTS #

require 'pg'
#require 'bcrypt'
load "./local_env.rb" if File.exists?("./local_env.rb")

# --------------------------------------------------------------------------- #
                    # DATABASE CONFIGURATION #

db_params = {
	host: ENV['host'],
	port: ENV['port'],
	dbname: ENV['dbname'],
	user: ENV['user'],
	password: ENV['password']
}

# ------------------------------------------------------------------------- #
                      # Instantiate Connections #
#
$db = PG::Connection.new(db_params)

# --------------------------------------------------------------------------- #
                         # INSERT CONTACT #
#
def insert_game(p1_name, p2_name, winner, date)
	$db.exec("INSERT INTO tic_tac_toe (p1_name, p2_name, winner, date) 
		VALUES 
		('#{p1_name}', '#{p2_name}', '#{winner}', '#{date}')")
end

# --------------------------------------------------------------------------- #
                             # UPDATE #

## This takes all the information and updates all columns to
## what ever was in the boxes of the individual form generated
## by the function that also pulls the rows from amazon.
def update_contact(id, p1_name, p2_name, winner, date)
	query = "UPDATE tic_tac_toe
		 SET p1_name='#{p1_name}',
		     p2_name='#{p2_name}',
		     winner='#{winner}',
		     date='#{date}',
		     WHERE id=#{id.to_i};"
	$db.exec(query)
end

# --------------------------------------------------------------------------- #
                             # SEARCH #

def prep_query(info_hash)
	# INPUTS FROM SEARCH FORM
	first_name = info_hash[:first_name] # FIRST NAME
	phone = info_hash[:phone]           # PHONE NUMBER

  # If they enter a first name and lastname- SEARCH WITH BOTH
	if first_name != '' && phone != ''
		"SELECT * FROM contacts WHERE first_name='#{first_name}' AND phone='#{phone}'"
	# OR ELSE IF IT WAS JUST THE FIRST NAME- SEARCH WITH JUST THE FIRST NAME
	elsif first_name != ''
		"SELECT * FROM contacts WHERE first_name='#{first_name}'"
  # OR ELSE IF it WAS JUST THE SEARCH WITH JUST THE PHONE NUMBER, SEARCH WITH JUST THE PHONE NUMBER
	elsif phone != ''
		"SELECT * FROM contacts WHERE phone='#{phone}'"
	# IF NOT BOTH NOR EITHER ONE PROMT THE USER OF THIS
	else
		"SELECT * FROM contacts"
	end
end

# ------------------------------------- #
# RUNS SQL QUERY AND RETURNS DATA OBJECT
def response_obj(query)
	$db.exec(query)
end
# ------------------------------------- #
# PREPARE HTML TABLE
def prep_html(response_obj)
	html = ''
	html << "<table>
	<tr>
	    <td>ID</td>
	    <td>First Name</td>
	    <td>Last Name</td>
	    <td>Street Address</td>
	    <td>City</td>
	    <td>State</td>
	    <td>Zipcode</td>
	    <td>Phone Number</td>
	  </tr>"

	# GENERATE ROW
  response_obj.each do |row|
		html << "\t<tr>"
		row.each {|cell| html << "\t\t<td>#{cell[1]}</td>\n"}
		html << "\t</tr>"
	end
	# END TABLE
	html << "</table>"
	# RETURN FINAL STRING
	html
end

# TIE THEM ALL TOGETHER
def full_search_table_render(form_input_hash)
	prep_html(response_obj(prep_query(form_input_hash)))
end
# --------------------------------------------------------------------------- #
                              # DELETE #
# can add button in show all contacts table
def delete_contact(id)
	"DELETE FROM contacts WHERE ID = #{id}"
end
# ------------------------------------------------------------------------- #
												# CHECK IF ALREADY EXISTS #
#
def entry_exists(column, value)
	rows = []
	$db.exec("SELECT * FROM contacts WHERE #{column}='#{value}'").each {|row| rows << row}
	rows.empty? == false
end

def get_password(username)
	$db.exec("SELECT * FROM contacts WHERE username='#{username}'")[0]['password']
end

# ------------------------------------------------------------------------- #
# HANDLE REGISTRATION #
def handle_registration(username, password)
	# Check if the item is already in the database
	# If it's not in the db...
	if entry_exists('username', username) == false
	# hash the password
		hashed_pw = BCrypt::Password.create(password, :cost => 10)
		# insert the username and hashed password into the db
		$db.exec("INSERT INTO contacts (username, password) VALUES ('#{username}', '#{hashed_pw}')")
		session[:username] = username
		redirect :show_all_contacts
	else
		"#{username} is already taken."
	end
end
# ------------------------------------------------------------------------- #
											# HANDLE LOGIN #
def handle_login(username, password)
	# If the username is in the database...
	if entry_exists('username', username)
		# if the password is correct...
		if BCrypt::Password.new(get_password(username)) == password
		# set the username as a session VARIABLE (can this be donw outside of the app.rb?)
		session[:username] = username
		redirect :show_all_contacts
		else
		"Password is incorrect"
		end
	else
		"#{username} does not exist. "
	end
end


# --------------------------------------------------------------------------- #
def handle_logout()
	if session.key?('username')
		session.delete('username')
		redirect :login
	else
		"Not logged in"
	end
end
###############################################################################

                           # INLINE TESTS #
# ----------------------------------------- #
#puts(prep_query({phone: '3044449994', first_name: 'patrick'}))
#update_contact(1, 'Patrick', 'Mckowen', 'street', 'city', 'state', 'zipcode', 'phone')
# ----------------------------------------- #
#print($db.exec("SELECT * FROM contacts").class)
# ----------------------------------------- #
=begin
$db.exec("SELECT * FROM contacts").each do |item|
	puts item.to_a
end
=end
# ----------------------------------------- #
=begin
$db.exec("SELECT * FROM contacts").each do |item|
	item.each {|i| puts i}
end
=end
# ----------------------------------------- #

#print(prep_html(response_obj(prep_query({phone: '3044449994', first_name: ''}))))
#print(full_search_table_render({first_name: 'firsty', phone: '1-304-555-5555'}))
